//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Projekt_Hotel.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Rodzaj_masazu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rodzaj_masazu()
        {
            this.Rezerwacja_masazu = new HashSet<Rezerwacja_masazu>();
        }

        public Rodzaj_masazu(int rodzaj_id, string nazwa, double wartosc)
        {
            Rodzaj_id = rodzaj_id;
            Nazwa = nazwa;
            Wartosc = wartosc;
        }

        public int Rodzaj_id { get; set; }
        public string Nazwa { get; set; }
        public double Wartosc { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rezerwacja_masazu> Rezerwacja_masazu { get; set; }
    }
}
