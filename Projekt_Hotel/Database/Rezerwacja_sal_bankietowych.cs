//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Projekt_Hotel.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Rezerwacja_sal_bankietowych
    {
        public Rezerwacja_sal_bankietowych(int rezerwacja_sali_id, DateTime data_rezerwacji, DateTime data_przybycia, DateTime data_zakonczenia, int klient_id, int status_id, int sala_id, double wartosc)
        {
            Rezerwacja_sali_id = rezerwacja_sali_id;
            Data_rezerwacji = data_rezerwacji;
            Data_przybycia = data_przybycia;
            Data_zakonczenia = data_zakonczenia;
            Klient_id = klient_id;
            Status_id = status_id;
            Sala_id = sala_id;
            Wartosc = wartosc;
        }

        public Rezerwacja_sal_bankietowych()
        {
        }

        public int Rezerwacja_sali_id { get; set; }
        public System.DateTime Data_rezerwacji { get; set; }
        public System.DateTime Data_przybycia { get; set; }
        public System.DateTime Data_zakonczenia { get; set; }
        public int Klient_id { get; set; }
        public int Status_id { get; set; }
        public int Sala_id { get; set; }
        public double Wartosc { get; set; }
    
        public virtual Klient Klient { get; set; }
        public virtual Sale_bankietowe Sale_bankietowe { get; set; }
        public virtual Status Status { get; set; }
    }
}
