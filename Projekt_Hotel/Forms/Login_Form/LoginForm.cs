﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form;
using Projekt_Hotel.Forms.Hotel_Form.After_Notification_Change;
using Projekt_Hotel.Forms.Hotel_Form.TimerNotification;
using Projekt_Hotel.Forms.Massage_Form;

namespace Projekt_Hotel.Forms.Login_Form
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {

            InitializeComponent();
            passwordTextBox.PasswordChar = '*';
            labelInvalidLoginForm.Hide();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {



        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            String loginInto = FormState.NameOfPreviousButton;

            if (LoginTextBox.Text != null && passwordTextBox.Text != null)
            {
                var user = context.Accounts.Where(u => u.username.Equals(LoginTextBox.Text)).FirstOrDefault();


                    switch (loginInto)
                    {
                        case "Hotel":
                            if (user.username.Equals("pracownikHotelu") || user.username.Equals("admin"))
                            {
                                if (passwordTextBox.Text.Equals(user.password))
                                {

                                var getReservations = context.Rezerwacja_pokoi.ToList();
                                    foreach (var x in getReservations)
                                    {
                                        if (x.Status_id == 1 || x.Status_id == 2)
                                        {
                                        TimerForHotelReser timer = new TimerForHotelReser(x.Rezerwacja_pokoju_id);
                                        timer.startTimer();
                                        }
                                    }
                                    HotelForm h = new HotelForm();
                                    FormState.NameOfLoggedPerson = user.username;
                                    this.Hide();
                                    h.Closed += (s, args) => this.Close();
                                    h.Show();
                                }
                            }
                            break;
                        case "Masaże":

                            if (user.username.Equals("pracownikMasazu") || user.username.Equals("admin"))
                            {
                                if (passwordTextBox.Text.Equals(user.password))
                                {
                                    MassageForm m = new MassageForm();
                                    FormState.NameOfLoggedPerson = LoginTextBox.Text;
                                    this.Hide();
                                    m.Closed += (s, args) => this.Close();
                                    m.Show();
                                }
                            }
                            break;
                        default:
                            LoginTextBox.Text = "";
                            passwordTextBox.Text = "";
                            labelInvalidLoginForm.Show();
                            break;
                    }


                    LoginTextBox.Text = "";
                    passwordTextBox.Text = "";
                    labelInvalidLoginForm.Show();
                
            }
        }
    }
}
