﻿namespace Projekt_Hotel.Forms.Massage_Form
{
    partial class MassageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewReservation = new System.Windows.Forms.Button();
            this.btnReservationMassageList = new System.Windows.Forms.Button();
            this.btnGoBack = new System.Windows.Forms.Button();
            this.labelLoggedPerson = new System.Windows.Forms.Label();
            this.btnClientList = new System.Windows.Forms.Button();
            this.btnAddOffer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNewReservation
            // 
            this.btnNewReservation.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnNewReservation.Location = new System.Drawing.Point(259, 117);
            this.btnNewReservation.Name = "btnNewReservation";
            this.btnNewReservation.Size = new System.Drawing.Size(87, 44);
            this.btnNewReservation.TabIndex = 0;
            this.btnNewReservation.Text = "Nowa Rezerwacja";
            this.btnNewReservation.UseVisualStyleBackColor = false;
            this.btnNewReservation.Click += new System.EventHandler(this.btnNewReservation_Click);
            // 
            // btnReservationMassageList
            // 
            this.btnReservationMassageList.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnReservationMassageList.Location = new System.Drawing.Point(28, 119);
            this.btnReservationMassageList.Name = "btnReservationMassageList";
            this.btnReservationMassageList.Size = new System.Drawing.Size(87, 42);
            this.btnReservationMassageList.TabIndex = 1;
            this.btnReservationMassageList.Text = "Lista rezerwacji";
            this.btnReservationMassageList.UseVisualStyleBackColor = false;
            this.btnReservationMassageList.Click += new System.EventHandler(this.btnReservationMassageList_Click);
            // 
            // btnGoBack
            // 
            this.btnGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.btnGoBack.Location = new System.Drawing.Point(388, 253);
            this.btnGoBack.Name = "btnGoBack";
            this.btnGoBack.Size = new System.Drawing.Size(75, 23);
            this.btnGoBack.TabIndex = 2;
            this.btnGoBack.Text = "Powrót";
            this.btnGoBack.UseVisualStyleBackColor = false;
            this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
            // 
            // labelLoggedPerson
            // 
            this.labelLoggedPerson.AutoSize = true;
            this.labelLoggedPerson.Location = new System.Drawing.Point(256, 9);
            this.labelLoggedPerson.Name = "labelLoggedPerson";
            this.labelLoggedPerson.Size = new System.Drawing.Size(35, 13);
            this.labelLoggedPerson.TabIndex = 3;
            this.labelLoggedPerson.Text = "label1";
            // 
            // btnClientList
            // 
            this.btnClientList.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnClientList.Location = new System.Drawing.Point(143, 117);
            this.btnClientList.Name = "btnClientList";
            this.btnClientList.Size = new System.Drawing.Size(87, 42);
            this.btnClientList.TabIndex = 4;
            this.btnClientList.Text = "Lista Klientów";
            this.btnClientList.UseVisualStyleBackColor = false;
            this.btnClientList.Click += new System.EventHandler(this.btnClientList_Click);
            // 
            // btnAddOffer
            // 
            this.btnAddOffer.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnAddOffer.Location = new System.Drawing.Point(376, 117);
            this.btnAddOffer.Name = "btnAddOffer";
            this.btnAddOffer.Size = new System.Drawing.Size(87, 44);
            this.btnAddOffer.TabIndex = 5;
            this.btnAddOffer.Text = "Dodaj ofertę";
            this.btnAddOffer.UseVisualStyleBackColor = false;
            this.btnAddOffer.Click += new System.EventHandler(this.btnAddOffer_Click);
            // 
            // MassageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(497, 308);
            this.Controls.Add(this.btnAddOffer);
            this.Controls.Add(this.btnClientList);
            this.Controls.Add(this.labelLoggedPerson);
            this.Controls.Add(this.btnGoBack);
            this.Controls.Add(this.btnReservationMassageList);
            this.Controls.Add(this.btnNewReservation);
            this.Name = "MassageForm";
            this.Text = "MassageForm";
            this.Load += new System.EventHandler(this.MassageForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNewReservation;
        private System.Windows.Forms.Button btnReservationMassageList;
        private System.Windows.Forms.Button btnGoBack;
        private System.Windows.Forms.Label labelLoggedPerson;
        private System.Windows.Forms.Button btnClientList;
        private System.Windows.Forms.Button btnAddOffer;
    }
}