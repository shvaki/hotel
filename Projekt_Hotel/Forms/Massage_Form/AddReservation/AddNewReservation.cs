﻿using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_Hotel.Forms.AddReservation
{
    public partial class AddNewReservation : Form
    {
        public AddNewReservation()
        {
            InitializeComponent();
        }
        private void AddNewReservation_Load_1(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();

            dateTimePickerRez.Format = DateTimePickerFormat.Custom;
            dateTimePickerRez.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerZak.Format = DateTimePickerFormat.Custom;
            dateTimePickerZak.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerEnd.Format = DateTimePickerFormat.Custom;
            dateTimePickerEnd.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            textBoxPhoneNumber.Mask = "000-000-000";

            var massageTypes = context.Rodzaj_masazu.ToList();
            var reservations = context.Rezerwacja_masazu.ToList();

            var numbersOfMassageTypes = new List<Tuple<int, String>>();
            foreach (var x in massageTypes)
            {
                numbersOfMassageTypes.Add(new Tuple<int, String>(x.Rodzaj_id, x.Nazwa));

            }

            comboBoxMassageType.Items.AddRange(numbersOfMassageTypes.ToArray());
        }

        private void btnAddReservation_Click(object sender, EventArgs e)
        {
            //validation
            if (textBoxName.Text.Length <= 0 || textBoxName.Text.Length >= 50)
            {
                labelErrorMassage.Text = "Nieprawdiłowe imie";
                labelErrorMassage.Show();
            }
            else if (textBoxSurname.Text.Length <= 0 || textBoxSurname.Text.Length >= 50)
            {
                labelErrorMassage.Text = "Nieprawdiłowe nazwisko";
                labelErrorMassage.Show();
            }
            else if ((textBoxAdress.Text == null || textBoxAdress.Text.Length <= 0) && textBoxAdress.Enabled == true)
            {
                labelErrorMassage.Text = "Nieprawdiłowy adres";
                labelErrorMassage.Show();
            }
            else if ((textBoxPhoneNumber.Text.Length <= 0 || textBoxPhoneNumber.Text.Length > 11) && textBoxPhoneNumber.Enabled == true)
            {
                labelErrorMassage.Text = "Nieprawdiłowy numer telefonu";
                labelErrorMassage.Show();
            }
            else
            {
                //success
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var reservations = context.Rezerwacja_masazu.ToList();
                var clients = context.Klient.ToList();
                

                Boolean addedReservation = false;


                foreach (var x in clients)
                {
                    if (x.Imie.Equals(textBoxName.Text) && x.Nazwisko.Equals(textBoxSurname.Text))
                    {
                        var numberOfReservation = comboBoxMassageType.SelectedItem.ToString().Split(',');
                        var idTypes = numberOfReservation[0].Substring(0 + 1);
                        int idTypes_inInt = Int32.Parse(idTypes);
                        var massage = context.Rodzaj_masazu.Where(i => i.Rodzaj_id == idTypes_inInt).FirstOrDefault();

                        var hour_diff = dateTimePickerEnd.Value - dateTimePickerZak.Value;
                        double payment = 0;
                        if (hour_diff.Hours == 0)
                        {
                            payment = massage.Wartosc * 1;
                        }
                        else
                        {
                            payment = massage.Wartosc * (hour_diff.Hours + 1);
                        }
                        double? realPayment = 0;
                        if (checkBoxCard.Checked)
                            realPayment = payment - (payment * 0.3);
                        else
                            realPayment = payment;
                        var tmp = Convert.ToDouble(realPayment);

                        context.Rezerwacja_masazu.Add(new Rezerwacja_masazu(
                            clients.Count+1,
                            massage.Rodzaj_id,
                            tmp,
                            dateTimePickerRez.Value,
                            dateTimePickerZak.Value,
                            dateTimePickerEnd.Value,
                            x.Klient_id
                            ));                       
                        context.SaveChanges();
                        MessageBox.Show("Pomyślnie dodano rezerwację! Cena rezerwacji to: " + realPayment+ " zł");
                        addedReservation = true;
                        FormState.goToMassageForm.Show();
                        this.Hide();
                    }
                }
                if (!addedReservation)
                {
                    var Dk = context.Dane_Kontaktowe.ToList();
                    context.Dane_Kontaktowe.Add(new Dane_Kontaktowe(
                        Dk.Count + 1,
                        textBoxAdress.Text,
                        textBoxPhoneNumber.Text
                        ));
                    context.Klient.Add(new Klient(
                        clients.Count + 1,
                        textBoxName.Text,
                        textBoxSurname.Text,
                        Dk.Count + 1,
                        4//masaże
                        ));
                    context.SaveChanges();
                    clients = context.Klient.ToList();
                    var numberOfReservation = comboBoxMassageType.SelectedItem.ToString().Split(',');
                    var idTypes = numberOfReservation[0].Substring(0 + 1);
                    int idTypes_inInt = Int32.Parse(idTypes);
                    var massage = context.Rodzaj_masazu.Where(i => i.Rodzaj_id == idTypes_inInt).FirstOrDefault();

                    var hour_diff = dateTimePickerEnd.Value - dateTimePickerZak.Value;
                    double payment = 0;
                    if (hour_diff.Hours==0)
                    {
                         payment = massage.Wartosc * 1;
                    }
                    else
                    {
                         payment = massage.Wartosc * (hour_diff.Hours +1 );
                    }
                   
                    double? realPayment = 0;
                    if (checkBoxCard.Checked)
                        realPayment = payment - (payment * 0.3);
                    else
                        realPayment = payment;
                    var tmp = Convert.ToDouble(realPayment);
                   
               
                    context.Rezerwacja_masazu.Add(new Rezerwacja_masazu(
                           clients.Count+1,
                           massage.Rodzaj_id,
                           tmp,
                           dateTimePickerRez.Value,
                           dateTimePickerZak.Value,
                           dateTimePickerEnd.Value,
                           clients[clients.Count-1].Klient_id
                           ));                
                    context.SaveChanges();
                    MessageBox.Show("Pomyślnie dodano rezerwację! Cena rezerwacji to: " + realPayment + " zł");
                    FormState.goToMassageForm.Show();
                    this.Hide();

                }       
            }
        }

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            FormState.goToMassageForm.Show();
            this.Hide();
        }

        private void textBoxSurname_TextChanged(object sender, EventArgs e)
        {
            if (textBoxName.Text.Length > 0)
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getGuy = context.Klient.Where(n => n.Nazwisko.Equals(textBoxSurname.Text) && n.Imie.Equals(textBoxName.Text) && n.Identity_id == 4).FirstOrDefault();
                if (getGuy != null)
                {
                    textBoxAdress.Hide();
                    textBoxPhoneNumber.Hide();
                    textBoxAdress.Enabled = false;
                    textBoxPhoneNumber.Enabled = false;
                }
                else
                {
                    textBoxAdress.Show();
                    textBoxPhoneNumber.Show();
                    textBoxAdress.Enabled = true;
                    textBoxPhoneNumber.Enabled = true;
                }
            }
        } 
    }
}
