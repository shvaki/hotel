﻿namespace Projekt_Hotel.Forms.AddReservation
{
    partial class AddNewReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGoBack = new System.Windows.Forms.Button();
            this.btnAddReservation = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.dateTimePickerRez = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerZak = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.LabelReservationName = new System.Windows.Forms.Label();
            this.labelReservationLastName = new System.Windows.Forms.Label();
            this.labelReservationDate = new System.Windows.Forms.Label();
            this.labelReservationArrivalDate = new System.Windows.Forms.Label();
            this.labelReservationEndDate = new System.Windows.Forms.Label();
            this.labelAdress = new System.Windows.Forms.Label();
            this.labelTelNumber = new System.Windows.Forms.Label();
            this.textBoxAdress = new System.Windows.Forms.TextBox();
            this.comboBoxMassageType = new System.Windows.Forms.ComboBox();
            this.labelMassageType = new System.Windows.Forms.Label();
            this.checkBoxCard = new System.Windows.Forms.CheckBox();
            this.labelErrorMassage = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // btnGoBack
            // 
            this.btnGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.btnGoBack.Location = new System.Drawing.Point(190, 464);
            this.btnGoBack.Name = "btnGoBack";
            this.btnGoBack.Size = new System.Drawing.Size(110, 51);
            this.btnGoBack.TabIndex = 1;
            this.btnGoBack.Text = "Powrót";
            this.btnGoBack.UseVisualStyleBackColor = false;
            this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
            // 
            // btnAddReservation
            // 
            this.btnAddReservation.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddReservation.Location = new System.Drawing.Point(42, 464);
            this.btnAddReservation.Name = "btnAddReservation";
            this.btnAddReservation.Size = new System.Drawing.Size(120, 51);
            this.btnAddReservation.TabIndex = 2;
            this.btnAddReservation.Text = "Dodaj rezerwację";
            this.btnAddReservation.UseVisualStyleBackColor = false;
            this.btnAddReservation.Click += new System.EventHandler(this.btnAddReservation_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(79, 29);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(210, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(79, 74);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(210, 20);
            this.textBoxSurname.TabIndex = 4;
            this.textBoxSurname.TextChanged += new System.EventHandler(this.textBoxSurname_TextChanged);
            // 
            // dateTimePickerRez
            // 
            this.dateTimePickerRez.Location = new System.Drawing.Point(79, 212);
            this.dateTimePickerRez.Name = "dateTimePickerRez";
            this.dateTimePickerRez.Size = new System.Drawing.Size(210, 20);
            this.dateTimePickerRez.TabIndex = 6;
            // 
            // dateTimePickerZak
            // 
            this.dateTimePickerZak.Location = new System.Drawing.Point(79, 262);
            this.dateTimePickerZak.Name = "dateTimePickerZak";
            this.dateTimePickerZak.Size = new System.Drawing.Size(210, 20);
            this.dateTimePickerZak.TabIndex = 7;
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.Location = new System.Drawing.Point(79, 313);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(210, 20);
            this.dateTimePickerEnd.TabIndex = 8;
            // 
            // LabelReservationName
            // 
            this.LabelReservationName.AutoSize = true;
            this.LabelReservationName.Location = new System.Drawing.Point(79, 13);
            this.LabelReservationName.Name = "LabelReservationName";
            this.LabelReservationName.Size = new System.Drawing.Size(29, 13);
            this.LabelReservationName.TabIndex = 9;
            this.LabelReservationName.Text = "Imię:";
            // 
            // labelReservationLastName
            // 
            this.labelReservationLastName.AutoSize = true;
            this.labelReservationLastName.Location = new System.Drawing.Point(79, 58);
            this.labelReservationLastName.Name = "labelReservationLastName";
            this.labelReservationLastName.Size = new System.Drawing.Size(56, 13);
            this.labelReservationLastName.TabIndex = 10;
            this.labelReservationLastName.Text = "Nazwisko:";
            // 
            // labelReservationDate
            // 
            this.labelReservationDate.AutoSize = true;
            this.labelReservationDate.Location = new System.Drawing.Point(79, 196);
            this.labelReservationDate.Name = "labelReservationDate";
            this.labelReservationDate.Size = new System.Drawing.Size(83, 13);
            this.labelReservationDate.TabIndex = 11;
            this.labelReservationDate.Text = "Data rezerwacji:";
            // 
            // labelReservationArrivalDate
            // 
            this.labelReservationArrivalDate.AutoSize = true;
            this.labelReservationArrivalDate.Location = new System.Drawing.Point(79, 246);
            this.labelReservationArrivalDate.Name = "labelReservationArrivalDate";
            this.labelReservationArrivalDate.Size = new System.Drawing.Size(80, 13);
            this.labelReservationArrivalDate.TabIndex = 12;
            this.labelReservationArrivalDate.Text = "Data przybycia:";
            // 
            // labelReservationEndDate
            // 
            this.labelReservationEndDate.AutoSize = true;
            this.labelReservationEndDate.Location = new System.Drawing.Point(79, 297);
            this.labelReservationEndDate.Name = "labelReservationEndDate";
            this.labelReservationEndDate.Size = new System.Drawing.Size(96, 13);
            this.labelReservationEndDate.TabIndex = 13;
            this.labelReservationEndDate.Text = "Data zakończenia:";
            // 
            // labelAdress
            // 
            this.labelAdress.AutoSize = true;
            this.labelAdress.Location = new System.Drawing.Point(79, 150);
            this.labelAdress.Name = "labelAdress";
            this.labelAdress.Size = new System.Drawing.Size(37, 13);
            this.labelAdress.TabIndex = 17;
            this.labelAdress.Text = "Adres:";
            // 
            // labelTelNumber
            // 
            this.labelTelNumber.AutoSize = true;
            this.labelTelNumber.Location = new System.Drawing.Point(79, 105);
            this.labelTelNumber.Name = "labelTelNumber";
            this.labelTelNumber.Size = new System.Drawing.Size(62, 13);
            this.labelTelNumber.TabIndex = 16;
            this.labelTelNumber.Text = "Nr telefonu:";
            // 
            // textBoxAdress
            // 
            this.textBoxAdress.Location = new System.Drawing.Point(79, 166);
            this.textBoxAdress.Name = "textBoxAdress";
            this.textBoxAdress.Size = new System.Drawing.Size(210, 20);
            this.textBoxAdress.TabIndex = 15;
            // 
            // comboBoxMassageType
            // 
            this.comboBoxMassageType.FormattingEnabled = true;
            this.comboBoxMassageType.Location = new System.Drawing.Point(82, 365);
            this.comboBoxMassageType.Name = "comboBoxMassageType";
            this.comboBoxMassageType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMassageType.TabIndex = 18;
            // 
            // labelMassageType
            // 
            this.labelMassageType.AutoSize = true;
            this.labelMassageType.Location = new System.Drawing.Point(79, 349);
            this.labelMassageType.Name = "labelMassageType";
            this.labelMassageType.Size = new System.Drawing.Size(67, 13);
            this.labelMassageType.TabIndex = 19;
            this.labelMassageType.Text = "Typ masażu:";
            // 
            // checkBoxCard
            // 
            this.checkBoxCard.AutoSize = true;
            this.checkBoxCard.Location = new System.Drawing.Point(79, 402);
            this.checkBoxCard.Name = "checkBoxCard";
            this.checkBoxCard.Size = new System.Drawing.Size(124, 17);
            this.checkBoxCard.TabIndex = 20;
            this.checkBoxCard.Text = "Karta stałego klienta";
            this.checkBoxCard.UseVisualStyleBackColor = true;
            // 
            // labelErrorMassage
            // 
            this.labelErrorMassage.AutoSize = true;
            this.labelErrorMassage.ForeColor = System.Drawing.Color.Red;
            this.labelErrorMassage.Location = new System.Drawing.Point(79, 433);
            this.labelErrorMassage.Name = "labelErrorMassage";
            this.labelErrorMassage.Size = new System.Drawing.Size(0, 13);
            this.labelErrorMassage.TabIndex = 21;
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(79, 122);
            this.textBoxPhoneNumber.Mask = "000-000-000";
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(210, 20);
            this.textBoxPhoneNumber.TabIndex = 22;
            // 
            // AddNewReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(370, 541);
            this.Controls.Add(this.textBoxPhoneNumber);
            this.Controls.Add(this.labelErrorMassage);
            this.Controls.Add(this.checkBoxCard);
            this.Controls.Add(this.labelMassageType);
            this.Controls.Add(this.comboBoxMassageType);
            this.Controls.Add(this.labelAdress);
            this.Controls.Add(this.labelTelNumber);
            this.Controls.Add(this.textBoxAdress);
            this.Controls.Add(this.labelReservationEndDate);
            this.Controls.Add(this.labelReservationArrivalDate);
            this.Controls.Add(this.labelReservationDate);
            this.Controls.Add(this.labelReservationLastName);
            this.Controls.Add(this.LabelReservationName);
            this.Controls.Add(this.dateTimePickerEnd);
            this.Controls.Add(this.dateTimePickerZak);
            this.Controls.Add(this.dateTimePickerRez);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.btnAddReservation);
            this.Controls.Add(this.btnGoBack);
            this.Name = "AddNewReservation";
            this.Text = "Dodaj Rezerwację";
            this.Load += new System.EventHandler(this.AddNewReservation_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnGoBack;
        private System.Windows.Forms.Button btnAddReservation;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.DateTimePicker dateTimePickerRez;
        private System.Windows.Forms.DateTimePicker dateTimePickerZak;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.Label LabelReservationName;
        private System.Windows.Forms.Label labelReservationLastName;
        private System.Windows.Forms.Label labelReservationDate;
        private System.Windows.Forms.Label labelReservationArrivalDate;
        private System.Windows.Forms.Label labelReservationEndDate;
        private System.Windows.Forms.Label labelAdress;
        private System.Windows.Forms.Label labelTelNumber;
        private System.Windows.Forms.TextBox textBoxAdress;
        private System.Windows.Forms.ComboBox comboBoxMassageType;
        private System.Windows.Forms.Label labelMassageType;
        private System.Windows.Forms.CheckBox checkBoxCard;
        private System.Windows.Forms.Label labelErrorMassage;
        private System.Windows.Forms.MaskedTextBox textBoxPhoneNumber;
    }
}