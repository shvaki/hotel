﻿using Projekt_Hotel.Classes;
using Projekt_Hotel.Forms.AddReservation;
using Projekt_Hotel.Forms.Massage_Form.Add_Offer;
using Projekt_Hotel.Forms.Massage_Form.List_Of_Clients;
using Projekt_Hotel.Forms.Massage_Form.ListOfReservation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_Hotel.Forms.Massage_Form
{
    public partial class MassageForm : Form
    {
        public MassageForm()
        {
            InitializeComponent();
        }
      
        private void btnGoBack_Click(object sender, EventArgs e)
        {
            FormState.PreviousPage.Show();
            this.Hide();
            FormState.PreviousPage = this;
        }

        private void btnNewReservation_Click(object sender, EventArgs e)
        {
            AddNewReservation addReservation = new AddNewReservation();
            this.Hide();
            addReservation.Closed += (s, args) => this.Close();
            addReservation.Show();
        }

        private void btnReservationMassageList_Click(object sender, EventArgs e)
        {
            ListaRezerwacji list = new ListaRezerwacji();
            this.Hide();
            list.Closed += (s, args) => this.Close();
            list.Show();
            

        }

        private void MassageForm_Load(object sender, EventArgs e)
        {
            labelLoggedPerson.Text = "Jestes zalogowany jako: " + FormState.NameOfLoggedPerson;
        }

        private void btnClientList_Click(object sender, EventArgs e)
        {
            ListOfClientsMassage list = new ListOfClientsMassage();
            this.Hide();
            list.Closed += (s, args) => this.Close();
            list.Show();
        }

        private void btnAddOffer_Click(object sender, EventArgs e)
        {
            AddOffer addOffer = new AddOffer();
            this.Hide();
            addOffer.Closed += (s, args) => this.Close();
            addOffer.Show();

        }
    }
}
