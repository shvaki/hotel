﻿using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_Hotel.Forms.Massage_Form.Add_Offer
{
    public partial class AddOffer : Form
    {
        public AddOffer()
        {
            InitializeComponent();
            labelError.Hide();
        }

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormState.goToMassageForm.Show();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text.Length <= 0 || textBoxName.Text.Length >= 25)
            {
                labelError.Text = "Niepoprawna nazwa masażu!";
                labelError.Show();

            }
            else if (numericUpDownPrice.Value <= 0)
            {
                labelError.Text = "Niepoprawna cena masażu!";
                labelError.Show();
            }
            else
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var massageTypes = context.Rodzaj_masazu.ToList();

                var tmpForHourVal = Convert.ToDouble(numericUpDownPrice.Value);
                var nazwa = textBoxName.Text;

                context.Rodzaj_masazu.Add(new Rodzaj_masazu(
                    massageTypes[massageTypes.Count - 1].Rodzaj_id + 1,
                    nazwa,
                    tmpForHourVal
                    ));
                context.SaveChanges();
                this.Hide();
                MessageBox.Show("Pomyślnie dodano nową ofertę!");
                FormState.goToMassageForm.Show();
            }

        }
    }
}
