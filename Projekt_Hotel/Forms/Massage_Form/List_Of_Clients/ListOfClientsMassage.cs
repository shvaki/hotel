﻿using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Clients.New_Client;
using Projekt_Hotel.Forms.Massage_Form.List_Of_Clients.Add_New_Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_Hotel.Forms.Massage_Form.List_Of_Clients
{
    public partial class ListOfClientsMassage : Form
    {
        public ListOfClientsMassage()
        {
            InitializeComponent();
            if (FormState.NameOfLoggedPerson != "admin")
                btnDeleteClient.Hide();
            ReloadList();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            FormState.goToMassageForm.Show();
            this.Hide();

        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                List<object> x2 = checkedListBox1.CheckedItems.OfType<object>().ToList();
                var x = x2.FirstOrDefault().ToString().Split(' ');
                var tmp = x[0];
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var singleClient = context.Klient.Where(n => n.Imie.Equals(tmp) && n.Identity_id == 4).FirstOrDefault();
                var singleClientDk = context.Dane_Kontaktowe.Where(dk => dk.Dane_kontaktowe_id == singleClient.Dane_kontaktowe_id).FirstOrDefault();
                MessageBox.Show("Dane o kliencie. Imie: " + singleClient.Imie + ". Nazwisko: " + singleClient.Nazwisko + ". Adres: " + singleClientDk.adres + ". Numer telefonu: " + singleClientDk.telefon);

            }
        }
        private void ListOfClientsMassage_Load(object sender, EventArgs e)
        {
            ReloadList();
        }
        private void ReloadList()
        {
            checkedListBox1.Items.Clear();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var clients = context.Klient.ToList();

            List<String> namesAndSurnames = new List<String>();
            foreach (var x in clients)
            {
                if (x.Identity_id == 4)
                {
                    String nameAndSurname = x.Imie + " " + x.Nazwisko;
                    namesAndSurnames.Add(nameAndSurname);
                }

            }
            checkedListBox1.Items.AddRange(namesAndSurnames.ToArray());
        }
        private void btnAddClient_Click(object sender, EventArgs e)
        {
            AddNewClient nc = new AddNewClient();
            this.Hide();
            FormState.PreviousPage = this;
            nc.Closed += (s, args) => this.Close();
            nc.Show();
        }

        private void btnDeleteClient_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                List<object> x2 = checkedListBox1.CheckedItems.OfType<object>().ToList();
                var x = x2.FirstOrDefault().ToString().Split(' ');
                var tmp = x[0];
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getIdOfClient = context.Klient.Where(n => n.Imie.Equals(tmp)).FirstOrDefault();
                var getIdOfDk = context.Dane_Kontaktowe.Where(dk => dk.Dane_kontaktowe_id == getIdOfClient.Dane_kontaktowe_id).FirstOrDefault();

                //delete
                context.Dane_Kontaktowe.Remove(getIdOfDk);
                context.Klient.Remove(getIdOfClient);
                context.SaveChanges();
                ReloadList();
            }
        }
    }
}
