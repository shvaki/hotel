﻿using Projekt_Hotel.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_Hotel.Forms.Massage_Form.List_Of_Clients.Add_New_Client
{
    public partial class AddNewClient : Form
    {
        public AddNewClient()
        {
            InitializeComponent();
            labelError.Hide();
            textBoxPhoneNr.Mask = "000-000-000";
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text.Length <= 0 || textBoxName.Text.Length >= 50)
            {
                labelError.Text = "Nieprawdiłowe imie";
                labelError.Show();
            }
            else if (textBoxSurname.Text.Length <= 0 || textBoxSurname.Text.Length >= 50)
            {
                labelError.Text = "Nieprawdiłowe nazwisko";
                labelError.Show();
            }
            else if (textBoxAddress.Text == null || textBoxAddress.Text.Length <= 0)
            {
                labelError.Text = "Nieprawdiłowy adres";
                labelError.Show();
            }
            else if (textBoxPhoneNr.Text.Length <= 0 || textBoxPhoneNr.Text.Length > 11)
            {
                labelError.Text = "Nieprawdiłowy numer telefonu";
                labelError.Show();
            }
            else
            {
                //success
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var lengthOfDK = context.Dane_Kontaktowe.ToList();
                var lengthOfClient = context.Klient.ToList();
                var listCount = lengthOfClient.Count;
                using (var newContext = new Hotel_u_ZbysiaDBEntities())
                {
                    newContext.Dane_Kontaktowe.Add(new Dane_Kontaktowe(
                            lengthOfDK.Count + 1, 
                            textBoxAddress.Text, 
                            textBoxPhoneNr.Text
                            ));
                    newContext.Klient.Add(new Klient(
                        listCount + 1, 
                        textBoxName.Text, 
                        textBoxSurname.Text, 
                        lengthOfDK.Count + 1,
                        4
                        ));
                    newContext.SaveChanges();
                }

                ListOfClientsMassage ls = new ListOfClientsMassage();
                ls.Show();
                this.Hide();
            }
        }
    }
}
