﻿namespace Projekt_Hotel.Forms.Massage_Form.ListOfReservation
{
    partial class ListaRezerwacji
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelReservationList = new System.Windows.Forms.Label();
            this.btnGoBack = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeaderID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderVal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRoomNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // labelReservationList
            // 
            this.labelReservationList.AutoSize = true;
            this.labelReservationList.Location = new System.Drawing.Point(35, 23);
            this.labelReservationList.Name = "labelReservationList";
            this.labelReservationList.Size = new System.Drawing.Size(82, 13);
            this.labelReservationList.TabIndex = 1;
            this.labelReservationList.Text = "Lista rezerwacji:";
            // 
            // btnGoBack
            // 
            this.btnGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.btnGoBack.Location = new System.Drawing.Point(138, 435);
            this.btnGoBack.Name = "btnGoBack";
            this.btnGoBack.Size = new System.Drawing.Size(75, 23);
            this.btnGoBack.TabIndex = 3;
            this.btnGoBack.Text = "Powrót";
            this.btnGoBack.UseVisualStyleBackColor = false;
            this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderID,
            this.columnHeaderVal,
            this.columnHeaderRoomNr});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(26, 51);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(322, 378);
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // columnHeaderID
            // 
            this.columnHeaderID.Text = "L.P";
            this.columnHeaderID.Width = 69;
            // 
            // columnHeaderVal
            // 
            this.columnHeaderVal.Text = "Imię i nazwisko";
            this.columnHeaderVal.Width = 143;
            // 
            // columnHeaderRoomNr
            // 
            this.columnHeaderRoomNr.Text = "Cena";
            this.columnHeaderRoomNr.Width = 140;
            // 
            // ListaRezerwacji
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(375, 481);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.btnGoBack);
            this.Controls.Add(this.labelReservationList);
            this.Name = "ListaRezerwacji";
            this.Text = "ListaRezerwacji";
            this.Load += new System.EventHandler(this.ListaRezerwacji_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelReservationList;
        private System.Windows.Forms.Button btnGoBack;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeaderID;
        private System.Windows.Forms.ColumnHeader columnHeaderVal;
        private System.Windows.Forms.ColumnHeader columnHeaderRoomNr;
    }
}