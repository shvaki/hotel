﻿using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.AddReservation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Forms.Massage_Form.ListOfReservation.Reservations_Details;

namespace Projekt_Hotel.Forms.Massage_Form.ListOfReservation
{
    public partial class ListaRezerwacji : Form
    {
        public ListaRezerwacji()
        {
            InitializeComponent();
            ReloadList();
        }

        private void ListaRezerwacji_Load(object sender, EventArgs e)
        {
            ReloadList();
        }

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormState.goToMassageForm.Show();
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                var tmp = Convert.ToInt32(item.SubItems[0].Text);
                ReservationsDetails rd = new ReservationsDetails(tmp);
                this.Hide();
                rd.Closed += (s, args) => this.Close();
                rd.Show();
            }
        }
        private void ReloadList()
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var reservations = context.Rezerwacja_masazu.ToList();
            listView1.Items.Clear();
            foreach (var x in reservations)
            {

                ListViewItem item = new ListViewItem(x.Rezerwacja_masazu_id.ToString());
                item.SubItems.Add(x.Klient.Imie.ToString() + " " + x.Klient.Nazwisko.ToString());
                item.SubItems.Add(x.Cena.ToString());
                listView1.Items.Add(item);
            }
        }
    }
}
