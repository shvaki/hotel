﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Massage_Form.ListOfReservation;
using Projekt_Hotel.Forms.Massage_Form.ListOfReservation.Reservations_Details;

namespace Projekt_Hotel.Forms.Massage_Form.ListOfReservation.Reservations_Details
{
    public partial class ReservationsDetails : Form
    {
        private int id;
        public ReservationsDetails(int uId)
        {
            this.id = uId;
            InitializeComponent();
           
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
                ListaRezerwacji lr = new ListaRezerwacji();
                this.Hide();
                lr.Show();        
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            dateTimePickerDateRez.Enabled = true;
            dateTimePickerDateZak.Enabled = true;
            dateTimePickerDateEnd.Enabled = true;
            buttonEdit.Hide();
            buttonAcceptEdit.Show();
        }

        private void ReservationsDetails_Load(object sender, EventArgs e)
        {
            buttonAcceptEdit.Hide();
            buttonAcceptTypesChanged.Hide();
            buttonAcceptEdit.Hide();
            comboBox1.Hide();
            labelSelectTypeMassage.Hide();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getReservation = context.Rezerwacja_masazu.Where(i => i.Rezerwacja_masazu_id == id).FirstOrDefault();

            dateTimePickerDateRez.Format = DateTimePickerFormat.Custom;
            dateTimePickerDateRez.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerDateZak.Format = DateTimePickerFormat.Custom;
            dateTimePickerDateZak.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerDateEnd.Format = DateTimePickerFormat.Custom;
            dateTimePickerDateEnd.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            textBoxNrRez.Text = getReservation.Rezerwacja_masazu_id.ToString();
            textBoxMassageType.Text = getReservation.Rodzaj_masazu.Nazwa.ToString();
            numericUpDownVal.Value = Convert.ToDecimal(getReservation.Cena);
            dateTimePickerDateRez.Value = getReservation.Data_rezerwacji;
            dateTimePickerDateZak.Value = getReservation.Data_przybycia;
            dateTimePickerDateEnd.Value = getReservation.Data_zakonczenia;

            textBoxNrRez.Enabled = false;
            dateTimePickerDateRez.Enabled = false;
            dateTimePickerDateZak.Enabled = false;
            dateTimePickerDateEnd.Enabled = false;
            textBoxMassageType.Enabled = false;
        }

        private void buttonAcceptEdit_Click(object sender, EventArgs e)
        {
            var diff = dateTimePickerDateZak.Value - dateTimePickerDateRez.Value;
            var diff2 = dateTimePickerDateEnd.Value - dateTimePickerDateZak.Value;
            if (diff.TotalSeconds > 0 && diff2.TotalSeconds > 0)
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getReservation = context.Rezerwacja_masazu.Where(i => i.Rezerwacja_masazu_id == id).FirstOrDefault();
                var massage = context.Rodzaj_masazu.Where(i => i.Rodzaj_id == getReservation.Rodzaj_id).FirstOrDefault();

                var hour_diff = dateTimePickerDateEnd.Value - dateTimePickerDateZak.Value;
                double payment = 0;
                if (hour_diff.Hours == 0)
              
                    payment = massage.Wartosc * 1;
                else
                    payment = massage.Wartosc * (hour_diff.Hours + 1);

                double? realPayment = 0;
                if (checkBoxCard.Checked)
                    realPayment = payment - (payment * 0.3);
                else
                    realPayment = payment;
                var tmp = Convert.ToDouble(realPayment);

                getReservation.Data_rezerwacji = dateTimePickerDateRez.Value;
                getReservation.Data_przybycia = dateTimePickerDateZak.Value;
                getReservation.Data_zakonczenia = dateTimePickerDateEnd.Value;
                getReservation.Cena = Convert.ToDouble(realPayment);
                context.SaveChanges();
            }
            ReservationsDetails rd = new ReservationsDetails(id);
            this.Hide();
            rd.Show();
        }

        private void buttonChangeType_Click(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var massageTypes = context.Rodzaj_masazu.ToList();

            var numbersOfMassageTypes = new List<Tuple<int, String>>();
            foreach (var x in massageTypes)
            {
                numbersOfMassageTypes.Add(new Tuple<int, String>(x.Rodzaj_id, x.Nazwa));

            }
            comboBox1.Items.AddRange(numbersOfMassageTypes.ToArray());

            comboBox1.Show();
            buttonAcceptTypesChanged.Show();
            labelSelectTypeMassage.Show();
            buttonChangeType.Hide();
            buttonEdit.Hide();

        }

        private void buttonAcceptTypesChanged_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                var diff2 = dateTimePickerDateEnd.Value - dateTimePickerDateZak.Value;

                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var numberOfReservation = comboBox1.SelectedItem.ToString().Split(',');
                var idTypes = numberOfReservation[0].Substring(0 + 1);
                int idTypes_inInt = Int32.Parse(idTypes);
                var getReservation = context.Rezerwacja_masazu.Where(i => i.Rezerwacja_masazu_id == id).FirstOrDefault();
                var massage = context.Rodzaj_masazu.Where(i => i.Rodzaj_id == getReservation.Rodzaj_id).FirstOrDefault();

                var hour_diff = dateTimePickerDateEnd.Value - dateTimePickerDateZak.Value;
                double payment = 0;
                if (hour_diff.Hours == 0)

                    payment = massage.Wartosc * 1;
                else
                    payment = massage.Wartosc * (hour_diff.Hours + 1);

                double? realPayment = 0;
                if (checkBoxCard.Checked)
                    realPayment = payment - (payment * 0.3);
                else
                    realPayment = payment;
                var tmp = Convert.ToDouble(realPayment);
                getReservation.Rodzaj_id = idTypes_inInt;
                getReservation.Cena = Convert.ToDouble(realPayment);
                context.SaveChanges();
            }

            ReservationsDetails rd = new ReservationsDetails(id);
            this.Hide();
            rd.Show();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getReservation = context.Rezerwacja_masazu.Where(i => i.Rezerwacja_masazu_id == id).FirstOrDefault();
            context.Rezerwacja_masazu.Remove(getReservation);
            context.SaveChanges();

            MessageBox.Show("Pomyślnie usunięto rezerwację!");

            ListaRezerwacji lr = new ListaRezerwacji();
            this.Hide();
            lr.Closed += (s, args) => this.Close();
            lr.Show();       
        }
    }
}
