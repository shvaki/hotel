﻿namespace Projekt_Hotel.Forms.Massage_Form.ListOfReservation.Reservations_Details
{
    partial class ReservationsDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAcceptEdit = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.numericUpDownVal = new System.Windows.Forms.NumericUpDown();
            this.dateTimePickerDateEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDateZak = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDateRez = new System.Windows.Forms.DateTimePicker();
            this.textBoxNrRez = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxMassageType = new System.Windows.Forms.ComboBox();
            this.checkBoxCard = new System.Windows.Forms.CheckBox();
            this.labelSelectTypeMassage = new System.Windows.Forms.Label();
            this.buttonAcceptTypesChanged = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonChangeType = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVal)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAcceptEdit
            // 
            this.buttonAcceptEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAcceptEdit.ForeColor = System.Drawing.Color.Blue;
            this.buttonAcceptEdit.Location = new System.Drawing.Point(370, 146);
            this.buttonAcceptEdit.Name = "buttonAcceptEdit";
            this.buttonAcceptEdit.Size = new System.Drawing.Size(140, 28);
            this.buttonAcceptEdit.TabIndex = 38;
            this.buttonAcceptEdit.Text = "Akceptuj edycje";
            this.buttonAcceptEdit.UseVisualStyleBackColor = true;
            this.buttonAcceptEdit.Click += new System.EventHandler(this.buttonAcceptEdit_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Location = new System.Drawing.Point(370, 86);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(140, 28);
            this.buttonEdit.TabIndex = 37;
            this.buttonEdit.Text = "Edytuj";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.buttonGoBack.Location = new System.Drawing.Point(370, 372);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(140, 28);
            this.buttonGoBack.TabIndex = 35;
            this.buttonGoBack.Text = "Powrót";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // numericUpDownVal
            // 
            this.numericUpDownVal.Location = new System.Drawing.Point(67, 105);
            this.numericUpDownVal.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownVal.Name = "numericUpDownVal";
            this.numericUpDownVal.ReadOnly = true;
            this.numericUpDownVal.Size = new System.Drawing.Size(205, 20);
            this.numericUpDownVal.TabIndex = 34;
            // 
            // dateTimePickerDateEnd
            // 
            this.dateTimePickerDateEnd.Location = new System.Drawing.Point(67, 311);
            this.dateTimePickerDateEnd.Name = "dateTimePickerDateEnd";
            this.dateTimePickerDateEnd.Size = new System.Drawing.Size(205, 20);
            this.dateTimePickerDateEnd.TabIndex = 33;
            // 
            // dateTimePickerDateZak
            // 
            this.dateTimePickerDateZak.Location = new System.Drawing.Point(67, 240);
            this.dateTimePickerDateZak.Name = "dateTimePickerDateZak";
            this.dateTimePickerDateZak.Size = new System.Drawing.Size(205, 20);
            this.dateTimePickerDateZak.TabIndex = 32;
            // 
            // dateTimePickerDateRez
            // 
            this.dateTimePickerDateRez.Location = new System.Drawing.Point(67, 168);
            this.dateTimePickerDateRez.Name = "dateTimePickerDateRez";
            this.dateTimePickerDateRez.Size = new System.Drawing.Size(205, 20);
            this.dateTimePickerDateRez.TabIndex = 31;
            // 
            // textBoxNrRez
            // 
            this.textBoxNrRez.Location = new System.Drawing.Point(67, 42);
            this.textBoxNrRez.Name = "textBoxNrRez";
            this.textBoxNrRez.Size = new System.Drawing.Size(205, 20);
            this.textBoxNrRez.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(63, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 24);
            this.label5.TabIndex = 25;
            this.label5.Text = "Data końca:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(63, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(190, 24);
            this.label4.TabIndex = 24;
            this.label4.Text = "Data zakwaterowania:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(63, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 24);
            this.label3.TabIndex = 23;
            this.label3.Text = "Data rezerwacji:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(63, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 24);
            this.label2.TabIndex = 22;
            this.label2.Text = "Cena:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(63, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 24);
            this.label1.TabIndex = 21;
            this.label1.Text = "Nr rezerwacji:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(63, 349);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 24);
            this.label7.TabIndex = 39;
            this.label7.Text = "Rodzaj masażu:";
            // 
            // textBoxMassageType
            // 
            this.textBoxMassageType.FormattingEnabled = true;
            this.textBoxMassageType.Location = new System.Drawing.Point(67, 377);
            this.textBoxMassageType.Name = "textBoxMassageType";
            this.textBoxMassageType.Size = new System.Drawing.Size(205, 21);
            this.textBoxMassageType.TabIndex = 40;
            // 
            // checkBoxCard
            // 
            this.checkBoxCard.AutoSize = true;
            this.checkBoxCard.Location = new System.Drawing.Point(92, 418);
            this.checkBoxCard.Name = "checkBoxCard";
            this.checkBoxCard.Size = new System.Drawing.Size(124, 17);
            this.checkBoxCard.TabIndex = 41;
            this.checkBoxCard.Text = "Karta stałego klienta";
            this.checkBoxCard.UseVisualStyleBackColor = true;
            // 
            // labelSelectTypeMassage
            // 
            this.labelSelectTypeMassage.AutoSize = true;
            this.labelSelectTypeMassage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSelectTypeMassage.Location = new System.Drawing.Point(350, 193);
            this.labelSelectTypeMassage.Name = "labelSelectTypeMassage";
            this.labelSelectTypeMassage.Size = new System.Drawing.Size(209, 24);
            this.labelSelectTypeMassage.TabIndex = 45;
            this.labelSelectTypeMassage.Text = "Wybierz rodzaj masażu:";
            // 
            // buttonAcceptTypesChanged
            // 
            this.buttonAcceptTypesChanged.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAcceptTypesChanged.ForeColor = System.Drawing.Color.Blue;
            this.buttonAcceptTypesChanged.Location = new System.Drawing.Point(374, 270);
            this.buttonAcceptTypesChanged.Name = "buttonAcceptTypesChanged";
            this.buttonAcceptTypesChanged.Size = new System.Drawing.Size(140, 43);
            this.buttonAcceptTypesChanged.TabIndex = 44;
            this.buttonAcceptTypesChanged.Text = "Akceptuj zmiane";
            this.buttonAcceptTypesChanged.UseVisualStyleBackColor = true;
            this.buttonAcceptTypesChanged.Click += new System.EventHandler(this.buttonAcceptTypesChanged_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(374, 237);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 43;
            // 
            // buttonChangeType
            // 
            this.buttonChangeType.Location = new System.Drawing.Point(374, 193);
            this.buttonChangeType.Name = "buttonChangeType";
            this.buttonChangeType.Size = new System.Drawing.Size(140, 28);
            this.buttonChangeType.TabIndex = 42;
            this.buttonChangeType.Text = "Zmień status";
            this.buttonChangeType.UseVisualStyleBackColor = true;
            this.buttonChangeType.Click += new System.EventHandler(this.buttonChangeType_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(370, 34);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(140, 28);
            this.buttonDelete.TabIndex = 46;
            this.buttonDelete.Text = "Usuń rezerwację";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // ReservationsDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(570, 450);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.labelSelectTypeMassage);
            this.Controls.Add(this.buttonAcceptTypesChanged);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.buttonChangeType);
            this.Controls.Add(this.checkBoxCard);
            this.Controls.Add(this.textBoxMassageType);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.buttonAcceptEdit);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.numericUpDownVal);
            this.Controls.Add(this.dateTimePickerDateEnd);
            this.Controls.Add(this.dateTimePickerDateZak);
            this.Controls.Add(this.dateTimePickerDateRez);
            this.Controls.Add(this.textBoxNrRez);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ReservationsDetails";
            this.Text = "ReservationsDetails";
            this.Load += new System.EventHandler(this.ReservationsDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonAcceptEdit;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.NumericUpDown numericUpDownVal;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateEnd;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateZak;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateRez;
        private System.Windows.Forms.TextBox textBoxNrRez;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox textBoxMassageType;
        private System.Windows.Forms.CheckBox checkBoxCard;
        private System.Windows.Forms.Label labelSelectTypeMassage;
        private System.Windows.Forms.Button buttonAcceptTypesChanged;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonChangeType;
        private System.Windows.Forms.Button buttonDelete;
    }
}