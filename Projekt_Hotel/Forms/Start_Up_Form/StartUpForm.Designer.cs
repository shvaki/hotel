﻿namespace Projekt_Hotel.Forms.Start_Up_Form
{
    partial class StartUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnHotel = new System.Windows.Forms.Button();
			this.btnMassage = new System.Windows.Forms.Button();
			this.buttonExit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnHotel
			// 
			this.btnHotel.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btnHotel.Location = new System.Drawing.Point(82, 31);
			this.btnHotel.Name = "btnHotel";
			this.btnHotel.Size = new System.Drawing.Size(176, 29);
			this.btnHotel.TabIndex = 0;
			this.btnHotel.Text = "Hotel";
			this.btnHotel.UseVisualStyleBackColor = false;
			this.btnHotel.Click += new System.EventHandler(this.btnHotel_Click_1);
			// 
			// btnMassage
			// 
			this.btnMassage.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btnMassage.Location = new System.Drawing.Point(82, 66);
			this.btnMassage.Name = "btnMassage";
			this.btnMassage.Size = new System.Drawing.Size(176, 29);
			this.btnMassage.TabIndex = 2;
			this.btnMassage.Text = "Masaże";
			this.btnMassage.UseVisualStyleBackColor = false;
			this.btnMassage.Click += new System.EventHandler(this.btnMassage_Click);
			// 
			// buttonExit
			// 
			this.buttonExit.BackColor = System.Drawing.Color.PaleVioletRed;
			this.buttonExit.Location = new System.Drawing.Point(128, 359);
			this.buttonExit.Name = "buttonExit";
			this.buttonExit.Size = new System.Drawing.Size(75, 23);
			this.buttonExit.TabIndex = 4;
			this.buttonExit.Text = "Wyjdź";
			this.buttonExit.UseVisualStyleBackColor = false;
			this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
			// 
			// StartUpForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.ClientSize = new System.Drawing.Size(348, 438);
			this.Controls.Add(this.buttonExit);
			this.Controls.Add(this.btnMassage);
			this.Controls.Add(this.btnHotel);
			this.Name = "StartUpForm";
			this.Text = "Menu";
			this.Load += new System.EventHandler(this.StartUpForm_Load);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnHotel;
        private System.Windows.Forms.Button btnMassage;
        private System.Windows.Forms.Button buttonExit;
    }
}