﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Forms.Login_Form;

namespace Projekt_Hotel.Forms.Start_Up_Form
{
    public partial class StartUpForm : Form
    {
        public StartUpForm()
        {
            InitializeComponent();
        }


        private void ShowLoginForm(String buttonName)
        {
            LoginForm l = new LoginForm();
            this.Hide();
            FormState.PreviousPage = this;
            FormState.NameOfPreviousButton = buttonName;
            l.Closed += (s, args) => this.Close();
            l.Show();
        }

        private void btnHotel_Click_1(object sender, EventArgs e)
        {
            ShowLoginForm(btnHotel.Text);
        }
        private void btnMassage_Click(object sender, EventArgs e)
        {
            ShowLoginForm(btnMassage.Text);
        }
        private void StartUpForm_Load(object sender, EventArgs e)
        {

        }
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
