﻿namespace Projekt_Hotel.Forms.Hotel_Form
{
    partial class HotelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnListReservations = new System.Windows.Forms.Button();
            this.btnListOfClients = new System.Windows.Forms.Button();
            this.btnListOfRooms = new System.Windows.Forms.Button();
            this.btnListOfEq = new System.Windows.Forms.Button();
            this.btnAddReservation = new System.Windows.Forms.Button();
            this.btnGoBack = new System.Windows.Forms.Button();
            this.labelLoggedPerson = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnListReservations
            // 
            this.btnListReservations.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnListReservations.Location = new System.Drawing.Point(52, 47);
            this.btnListReservations.Name = "btnListReservations";
            this.btnListReservations.Size = new System.Drawing.Size(96, 36);
            this.btnListReservations.TabIndex = 0;
            this.btnListReservations.Text = "Lista rezerwacji";
            this.btnListReservations.UseVisualStyleBackColor = false;
            this.btnListReservations.Click += new System.EventHandler(this.btnListReservations_Click);
            // 
            // btnListOfClients
            // 
            this.btnListOfClients.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnListOfClients.Location = new System.Drawing.Point(52, 118);
            this.btnListOfClients.Name = "btnListOfClients";
            this.btnListOfClients.Size = new System.Drawing.Size(96, 36);
            this.btnListOfClients.TabIndex = 1;
            this.btnListOfClients.Text = "Lista klientów";
            this.btnListOfClients.UseVisualStyleBackColor = false;
            this.btnListOfClients.Click += new System.EventHandler(this.btnListOfClients_Click);
            // 
            // btnListOfRooms
            // 
            this.btnListOfRooms.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnListOfRooms.Location = new System.Drawing.Point(52, 196);
            this.btnListOfRooms.Name = "btnListOfRooms";
            this.btnListOfRooms.Size = new System.Drawing.Size(96, 36);
            this.btnListOfRooms.TabIndex = 2;
            this.btnListOfRooms.Text = "Lista pokoi";
            this.btnListOfRooms.UseVisualStyleBackColor = false;
            this.btnListOfRooms.Click += new System.EventHandler(this.btnListOfRooms_Click);
            // 
            // btnListOfEq
            // 
            this.btnListOfEq.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnListOfEq.Location = new System.Drawing.Point(52, 275);
            this.btnListOfEq.Name = "btnListOfEq";
            this.btnListOfEq.Size = new System.Drawing.Size(96, 36);
            this.btnListOfEq.TabIndex = 3;
            this.btnListOfEq.Text = "Lista wyposażenia";
            this.btnListOfEq.UseVisualStyleBackColor = false;
            this.btnListOfEq.Click += new System.EventHandler(this.btnListOfEq_Click);
            // 
            // btnAddReservation
            // 
            this.btnAddReservation.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnAddReservation.Location = new System.Drawing.Point(219, 47);
            this.btnAddReservation.Name = "btnAddReservation";
            this.btnAddReservation.Size = new System.Drawing.Size(96, 36);
            this.btnAddReservation.TabIndex = 4;
            this.btnAddReservation.Text = "Dodaj rezerwacje";
            this.btnAddReservation.UseVisualStyleBackColor = false;
            this.btnAddReservation.Click += new System.EventHandler(this.btnAddReservation_Click);
            // 
            // btnGoBack
            // 
            this.btnGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.btnGoBack.Location = new System.Drawing.Point(397, 404);
            this.btnGoBack.Name = "btnGoBack";
            this.btnGoBack.Size = new System.Drawing.Size(75, 23);
            this.btnGoBack.TabIndex = 5;
            this.btnGoBack.Text = "Powrót";
            this.btnGoBack.UseVisualStyleBackColor = false;
            this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
            // 
            // labelLoggedPerson
            // 
            this.labelLoggedPerson.AutoSize = true;
            this.labelLoggedPerson.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.labelLoggedPerson.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelLoggedPerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelLoggedPerson.Location = new System.Drawing.Point(204, 9);
            this.labelLoggedPerson.Name = "labelLoggedPerson";
            this.labelLoggedPerson.Size = new System.Drawing.Size(37, 15);
            this.labelLoggedPerson.TabIndex = 6;
            this.labelLoggedPerson.Text = "label1";
            this.labelLoggedPerson.Click += new System.EventHandler(this.labelLoggedPerson_Click);
            // 
            // HotelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(499, 450);
            this.Controls.Add(this.labelLoggedPerson);
            this.Controls.Add(this.btnGoBack);
            this.Controls.Add(this.btnAddReservation);
            this.Controls.Add(this.btnListOfEq);
            this.Controls.Add(this.btnListOfRooms);
            this.Controls.Add(this.btnListOfClients);
            this.Controls.Add(this.btnListReservations);
            this.Name = "HotelForm";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.HotelForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnListReservations;
        private System.Windows.Forms.Button btnListOfClients;
        private System.Windows.Forms.Button btnListOfRooms;
        private System.Windows.Forms.Button btnListOfEq;
        private System.Windows.Forms.Button btnAddReservation;
        private System.Windows.Forms.Button btnGoBack;
        private System.Windows.Forms.Label labelLoggedPerson;
    }
}