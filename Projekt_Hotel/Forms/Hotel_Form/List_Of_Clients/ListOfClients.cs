﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Clients.New_Client;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Clients
{
    public partial class ListOfClients : Form
    {
        public ListOfClients()
        {
            InitializeComponent();
            if (FormState.NameOfLoggedPerson != "admin")
                btnDeleteClient.Hide();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var clients = context.Klient.ToList();

            List<String> namesAndSurnames = new List<String>();
            foreach (var x in clients)
            {
                if (x.Identity_id == 1)
                {
                    String nameAndSurname = x.Imie + " " + x.Nazwisko;
                    namesAndSurnames.Add(nameAndSurname);
                }


            }
            checkedListBox1.Items.AddRange(namesAndSurnames.ToArray());
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            FormState.goToHotelForm.Show();
            this.Hide();
          
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                List<object> x2 = checkedListBox1.CheckedItems.OfType<object>().ToList();
                var x = x2.FirstOrDefault().ToString().Split(' ');
                var tmp = x[0];
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var singleClient = context.Klient.Where(n => n.Imie.Equals(tmp) && n.Identity_id==1).FirstOrDefault();
                var singleClientDk = context.Dane_Kontaktowe.Where(dk => dk.Dane_kontaktowe_id == singleClient.Dane_kontaktowe_id).FirstOrDefault();
                MessageBox.Show("Dane o kliencie. Imie: "+singleClient.Imie+". Nazwisko: "+singleClient.Nazwisko+". Adres: "+singleClientDk.adres+". Numer telefonu: "+singleClientDk.telefon);

            }
        }

        private void ListOfClients_Load(object sender, EventArgs e)
        {
            checkedListBox1.Items.Clear();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var clients = context.Klient.ToList();

            List<String> namesAndSurnames = new List<String>();
            foreach (var x in clients)
            {
                if (x.Identity_id == 1)
                {
                    String nameAndSurname = x.Imie + " " + x.Nazwisko;
                    namesAndSurnames.Add(nameAndSurname);
                }

            }
            checkedListBox1.Items.AddRange(namesAndSurnames.ToArray());
        }
        private void ReloadList() {
            checkedListBox1.Items.Clear();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var clients = context.Klient.ToList();

            List<String> namesAndSurnames = new List<String>();
            foreach (var x in clients)
            {
                if (x.Identity_id == 1)
                {
                    String nameAndSurname = x.Imie + " " + x.Nazwisko;
                    namesAndSurnames.Add(nameAndSurname);
                }

            }
            checkedListBox1.Items.AddRange(namesAndSurnames.ToArray());
        }
        private void btnAddClient_Click(object sender, EventArgs e)
        {
            NewClient nc = new NewClient();
            this.Hide();
            FormState.PreviousPage = this;
            nc.Closed += (s, args) => this.Close();
            nc.Show();
        }

        private void btnDeleteClient_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count > 0) {
                List<object> x2 = checkedListBox1.CheckedItems.OfType<object>().ToList();
                var x = x2.FirstOrDefault().ToString().Split(' ');
                var tmp = x[0];
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getIdOfClient = context.Klient.Where(n => n.Imie.Equals(tmp)).FirstOrDefault();
                var getIdOfDk = context.Dane_Kontaktowe.Where(dk => dk.Dane_kontaktowe_id == getIdOfClient.Dane_kontaktowe_id).FirstOrDefault();

                //delete
                context.Dane_Kontaktowe.Remove(getIdOfDk);
                context.Klient.Remove(getIdOfClient);
                context.SaveChanges();
                ReloadList();
            }
        }
    }
}
