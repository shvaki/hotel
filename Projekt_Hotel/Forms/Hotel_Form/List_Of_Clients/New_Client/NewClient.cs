﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Clients.New_Client
{
    public partial class NewClient : Form
    {
        public NewClient()
        {
            InitializeComponent();
            labelErrorName.Hide();
            labelErrorSurname.Hide();
            labelErrorPhoneNr.Hide();
            labelErrorAddress.Hide();
            textBoxPhoneNr.Mask = "000-000-000";

        }

        private void NewClient_Load(object sender, EventArgs e)
        {

        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            //validation
            if (textBoxName.Text.Length <= 0 || textBoxName.Text.Length >= 50)
            {
                labelErrorName.Text = "Nieprawdiłowe imie";
                labelErrorName.Show();
            }
            else if (textBoxSurname.Text.Length <= 0 || textBoxSurname.Text.Length >= 50)
            {
                labelErrorSurname.Text = "Nieprawdiłowe nazwisko";
                labelErrorSurname.Show();
            }
            else if (textBoxAddress.Text == null || textBoxAddress.Text.Length <= 0)
            {
                labelErrorAddress.Text = "Nieprawdiłowy adres";
                labelErrorAddress.Show();
            }
            else if (textBoxPhoneNr.Text.Length <= 0 || textBoxPhoneNr.Text.Length > 11)
            {
                labelErrorPhoneNr.Text = "Nieprawdiłowy numer telefonu";
                labelErrorPhoneNr.Show();
            }
            else
            {
                //success
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var lengthOfDK = context.Dane_Kontaktowe.ToList();
                var lengthOfClient = context.Klient.ToList();
                using (var newContext = new Hotel_u_ZbysiaDBEntities()) {
                    newContext.Dane_Kontaktowe.Add(new Dane_Kontaktowe(
                            lengthOfDK.Count + 1, textBoxAddress.Text, textBoxPhoneNr.Text
                            ));
                    newContext.Klient.Add(new Klient(
                        lengthOfClient.Count + 1, textBoxName.Text, textBoxSurname.Text, lengthOfDK.Count + 1,1
                        ));
                    newContext.SaveChanges();
                }

                ListOfClients ls = new ListOfClients();
                ls.Show();
                this.Hide();
               

            }
        }
    }
}
