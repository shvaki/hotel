﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Clients.New_Client
{
    partial class NewClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelPhoneNr = new System.Windows.Forms.Label();
            this.labelErrorName = new System.Windows.Forms.Label();
            this.labelErrorSurname = new System.Windows.Forms.Label();
            this.labelErrorAddress = new System.Windows.Forms.Label();
            this.labelErrorPhoneNr = new System.Windows.Forms.Label();
            this.btnAccept = new System.Windows.Forms.Button();
            this.textBoxPhoneNr = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(259, 56);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(198, 20);
            this.textBoxName.TabIndex = 0;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(259, 119);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(198, 20);
            this.textBoxSurname.TabIndex = 1;
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(259, 191);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(198, 20);
            this.textBoxAddress.TabIndex = 2;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(138, 52);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(50, 24);
            this.labelName.TabIndex = 4;
            this.labelName.Text = "Imie:";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSurname.Location = new System.Drawing.Point(138, 114);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(95, 24);
            this.labelSurname.TabIndex = 5;
            this.labelSurname.Text = "Nazwisko:";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAddress.Location = new System.Drawing.Point(138, 186);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(65, 24);
            this.labelAddress.TabIndex = 6;
            this.labelAddress.Text = "Adres:";
            // 
            // labelPhoneNr
            // 
            this.labelPhoneNr.AutoSize = true;
            this.labelPhoneNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPhoneNr.Location = new System.Drawing.Point(138, 260);
            this.labelPhoneNr.Name = "labelPhoneNr";
            this.labelPhoneNr.Size = new System.Drawing.Size(115, 24);
            this.labelPhoneNr.TabIndex = 7;
            this.labelPhoneNr.Text = "Nr Telefonu:";
            // 
            // labelErrorName
            // 
            this.labelErrorName.AutoSize = true;
            this.labelErrorName.ForeColor = System.Drawing.Color.Red;
            this.labelErrorName.Location = new System.Drawing.Point(256, 40);
            this.labelErrorName.Name = "labelErrorName";
            this.labelErrorName.Size = new System.Drawing.Size(0, 13);
            this.labelErrorName.TabIndex = 8;
            // 
            // labelErrorSurname
            // 
            this.labelErrorSurname.AutoSize = true;
            this.labelErrorSurname.ForeColor = System.Drawing.Color.Red;
            this.labelErrorSurname.Location = new System.Drawing.Point(266, 103);
            this.labelErrorSurname.Name = "labelErrorSurname";
            this.labelErrorSurname.Size = new System.Drawing.Size(0, 13);
            this.labelErrorSurname.TabIndex = 9;
            // 
            // labelErrorAddress
            // 
            this.labelErrorAddress.AutoSize = true;
            this.labelErrorAddress.ForeColor = System.Drawing.Color.Red;
            this.labelErrorAddress.Location = new System.Drawing.Point(266, 175);
            this.labelErrorAddress.Name = "labelErrorAddress";
            this.labelErrorAddress.Size = new System.Drawing.Size(0, 13);
            this.labelErrorAddress.TabIndex = 10;
            // 
            // labelErrorPhoneNr
            // 
            this.labelErrorPhoneNr.AutoSize = true;
            this.labelErrorPhoneNr.ForeColor = System.Drawing.Color.Red;
            this.labelErrorPhoneNr.Location = new System.Drawing.Point(266, 248);
            this.labelErrorPhoneNr.Name = "labelErrorPhoneNr";
            this.labelErrorPhoneNr.Size = new System.Drawing.Size(0, 13);
            this.labelErrorPhoneNr.TabIndex = 11;
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAccept.Location = new System.Drawing.Point(218, 355);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 23);
            this.btnAccept.TabIndex = 12;
            this.btnAccept.Text = "Zatwierdź";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // textBoxPhoneNr
            // 
            this.textBoxPhoneNr.Location = new System.Drawing.Point(259, 265);
            this.textBoxPhoneNr.Name = "textBoxPhoneNr";
            this.textBoxPhoneNr.Size = new System.Drawing.Size(198, 20);
            this.textBoxPhoneNr.TabIndex = 13;
            // 
            // NewClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(552, 451);
            this.Controls.Add(this.textBoxPhoneNr);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.labelErrorPhoneNr);
            this.Controls.Add(this.labelErrorAddress);
            this.Controls.Add(this.labelErrorSurname);
            this.Controls.Add(this.labelErrorName);
            this.Controls.Add(this.labelPhoneNr);
            this.Controls.Add(this.labelAddress);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxAddress);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.textBoxName);
            this.Name = "NewClient";
            this.Text = "Dodaj klienta";
            this.Load += new System.EventHandler(this.NewClient_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label labelPhoneNr;
        private System.Windows.Forms.Label labelErrorName;
        private System.Windows.Forms.Label labelErrorSurname;
        private System.Windows.Forms.Label labelErrorAddress;
        private System.Windows.Forms.Label labelErrorPhoneNr;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.MaskedTextBox textBoxPhoneNr;
    }
}