﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Reservations
{
    partial class ListOfReservations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeaderID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderVal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRoomNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderID,
            this.columnHeaderVal,
            this.columnHeaderRoomNr,
            this.columnHeaderStat});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 23);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(322, 378);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // columnHeaderID
            // 
            this.columnHeaderID.Text = "L.P";
            // 
            // columnHeaderVal
            // 
            this.columnHeaderVal.Text = "Cena";
            this.columnHeaderVal.Width = 68;
            // 
            // columnHeaderRoomNr
            // 
            this.columnHeaderRoomNr.Text = "Nr. Pokoju";
            // 
            // columnHeaderStat
            // 
            this.columnHeaderStat.Text = "Status";
            this.columnHeaderStat.Width = 129;
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.buttonGoBack.Location = new System.Drawing.Point(74, 436);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(188, 23);
            this.buttonGoBack.TabIndex = 1;
            this.buttonGoBack.Text = "Powrót";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // ListOfReservations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(346, 481);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.listView1);
            this.Name = "ListOfReservations";
            this.Text = "Lista rezerwacji";
            this.Load += new System.EventHandler(this.ListOfReservations_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeaderID;
        private System.Windows.Forms.ColumnHeader columnHeaderVal;
        private System.Windows.Forms.ColumnHeader columnHeaderRoomNr;
        private System.Windows.Forms.ColumnHeader columnHeaderStat;
        private System.Windows.Forms.Button buttonGoBack;
    }
}