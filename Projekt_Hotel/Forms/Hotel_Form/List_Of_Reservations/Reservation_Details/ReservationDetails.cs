﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Reservations.Reservation_Details
{
    public partial class ReservationDetails : Form
    {
        public int id;
        public ReservationDetails(int uId)
        {
            this.id = uId;
            InitializeComponent();
        }

        private void ReservationDetails_Load(object sender, EventArgs e)
        {
            //hids
            buttonAcceptStatusChanged.Hide();
            buttonAcceptEdit.Hide();
            comboBox1.Hide();
            labelSelectStatus.Hide();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getReservation = context.Rezerwacja_pokoi.Where(i => i.Rezerwacja_pokoju_id == id).FirstOrDefault();



            dateTimePickerDateRez.Format = DateTimePickerFormat.Custom;
            dateTimePickerDateRez.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerDateZak.Format = DateTimePickerFormat.Custom;
            dateTimePickerDateZak.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerDateEnd.Format = DateTimePickerFormat.Custom;
            dateTimePickerDateEnd.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            textBoxNrRez.Text = getReservation.Rezerwacja_pokoju_id.ToString();
            textBoxNrRoom.Text = getReservation.Pokoj_id.ToString();
            textBoxStatus.Text = Enum.GetName(typeof(EnumClass.Status),getReservation.Status_id);
            numericUpDownVal.Value = Convert.ToDecimal(getReservation.Wartosc);
            dateTimePickerDateRez.Value = getReservation.Data_rezerwacji;
            dateTimePickerDateZak.Value = getReservation.Data_zakwaterowania;
            dateTimePickerDateEnd.Value = getReservation.Data_zakonczenia;

            textBoxNrRez.Enabled = false;
            dateTimePickerDateRez.Enabled = false;
            dateTimePickerDateZak.Enabled = false;
            dateTimePickerDateEnd.Enabled = false;
            
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            dateTimePickerDateRez.Enabled = true;
            dateTimePickerDateZak.Enabled = true;
            dateTimePickerDateEnd.Enabled = true;

            buttonChangeStatus.Hide();
            buttonEdit.Hide();

            buttonAcceptEdit.Show();
            
        }

        private void buttonAcceptEdit_Click(object sender, EventArgs e)
        {
            var diff = dateTimePickerDateZak.Value - dateTimePickerDateRez.Value;
            var diff2 = dateTimePickerDateEnd.Value - dateTimePickerDateZak.Value;
            if (diff.TotalSeconds > 0 && diff2.TotalSeconds > 0)
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getReservation = context.Rezerwacja_pokoi.Where(i => i.Rezerwacja_pokoju_id == id).FirstOrDefault();
                var room = context.Pokoj.Where(i => i.Pokoj_id == getReservation.Pokoj_id).FirstOrDefault();
                var client = context.Klient.Where(i => i.Klient_id == getReservation.Klient_id).FirstOrDefault();

                var payment = room.Wartosc * diff2.Days;
                double? realPayment = 0;
                if (client.Ilosc_wizyt_hotel > 0)
                    realPayment = payment / (client.Ilosc_wizyt_hotel * 0.01);
                else
                    realPayment = payment;
               

                getReservation.Data_rezerwacji= dateTimePickerDateRez.Value;
                getReservation.Data_zakwaterowania= dateTimePickerDateZak.Value;
                getReservation.Data_zakonczenia = dateTimePickerDateEnd.Value;
                getReservation.Wartosc = Convert.ToDouble(realPayment);
                context.SaveChanges();

            }


            ReservationDetails rd = new ReservationDetails(id);
            this.Hide();
            rd.Show();
        }

        private void buttonChangeStatus_Click(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getStatus = context.Status.ToList();
            foreach (var x in getStatus)
            {
                if (x.IdStatusu >= 1 && x.IdStatusu <= 4 || x.IdStatusu==10)
                {
                    comboBox1.Items.Add(x.Nazwa);
                }
            }
            comboBox1.Show();
            buttonAcceptStatusChanged.Show();
            labelSelectStatus.Show();
            buttonChangeStatus.Hide();
            buttonEdit.Hide();
            
        }

        private void buttonAcceptStatusChanged_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getReservation = context.Rezerwacja_pokoi.Where(i => i.Rezerwacja_pokoju_id == id).FirstOrDefault();
                var tmp = (int)Enum.Parse(typeof(EnumClass.Status), comboBox1.SelectedItem.ToString());

                if (tmp != 10)
                {
                    getReservation.Status_id = tmp;
                    context.SaveChanges();
                }
                else
                {
                    var getRoom = context.Pokoj.Where(i => i.Pokoj_id == getReservation.Pokoj_id).FirstOrDefault();
                    getRoom.Status_id = 5;
                    getReservation.Status_id = tmp;
                    context.SaveChanges();
                }

            }

            ReservationDetails rd = new ReservationDetails(id);
            this.Hide();
            rd.Show();
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            ListOfReservations lr = new ListOfReservations();
            this.Hide();
            lr.Show();
        }
    }
}
