﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Reservations.Reservation_Details
{
    partial class ReservationDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNrRez = new System.Windows.Forms.TextBox();
            this.textBoxNrRoom = new System.Windows.Forms.TextBox();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.dateTimePickerDateRez = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDateZak = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDateEnd = new System.Windows.Forms.DateTimePicker();
            this.numericUpDownVal = new System.Windows.Forms.NumericUpDown();
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.buttonChangeStatus = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAcceptEdit = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonAcceptStatusChanged = new System.Windows.Forms.Button();
            this.labelSelectStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVal)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nr rezerwacji:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(13, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cena:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(13, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data rezerwacji:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(13, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(190, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "Data zakwaterowania:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(13, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "Data końca:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(13, 325);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 24);
            this.label6.TabIndex = 5;
            this.label6.Text = "Status:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(13, 276);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 24);
            this.label7.TabIndex = 6;
            this.label7.Text = "Nr pokoju:";
            // 
            // textBoxNrRez
            // 
            this.textBoxNrRez.Location = new System.Drawing.Point(226, 27);
            this.textBoxNrRez.Name = "textBoxNrRez";
            this.textBoxNrRez.Size = new System.Drawing.Size(205, 20);
            this.textBoxNrRez.TabIndex = 7;
            // 
            // textBoxNrRoom
            // 
            this.textBoxNrRoom.Location = new System.Drawing.Point(226, 280);
            this.textBoxNrRoom.Name = "textBoxNrRoom";
            this.textBoxNrRoom.ReadOnly = true;
            this.textBoxNrRoom.Size = new System.Drawing.Size(205, 20);
            this.textBoxNrRoom.TabIndex = 8;
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Location = new System.Drawing.Point(226, 330);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.ReadOnly = true;
            this.textBoxStatus.Size = new System.Drawing.Size(205, 20);
            this.textBoxStatus.TabIndex = 9;
            // 
            // dateTimePickerDateRez
            // 
            this.dateTimePickerDateRez.Location = new System.Drawing.Point(226, 129);
            this.dateTimePickerDateRez.Name = "dateTimePickerDateRez";
            this.dateTimePickerDateRez.Size = new System.Drawing.Size(205, 20);
            this.dateTimePickerDateRez.TabIndex = 10;
            // 
            // dateTimePickerDateZak
            // 
            this.dateTimePickerDateZak.Location = new System.Drawing.Point(226, 183);
            this.dateTimePickerDateZak.Name = "dateTimePickerDateZak";
            this.dateTimePickerDateZak.Size = new System.Drawing.Size(205, 20);
            this.dateTimePickerDateZak.TabIndex = 11;
            // 
            // dateTimePickerDateEnd
            // 
            this.dateTimePickerDateEnd.Location = new System.Drawing.Point(226, 234);
            this.dateTimePickerDateEnd.Name = "dateTimePickerDateEnd";
            this.dateTimePickerDateEnd.Size = new System.Drawing.Size(205, 20);
            this.dateTimePickerDateEnd.TabIndex = 12;
            // 
            // numericUpDownVal
            // 
            this.numericUpDownVal.Location = new System.Drawing.Point(226, 76);
            this.numericUpDownVal.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownVal.Name = "numericUpDownVal";
            this.numericUpDownVal.ReadOnly = true;
            this.numericUpDownVal.Size = new System.Drawing.Size(205, 20);
            this.numericUpDownVal.TabIndex = 13;
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.buttonGoBack.Location = new System.Drawing.Point(601, 381);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(140, 28);
            this.buttonGoBack.TabIndex = 14;
            this.buttonGoBack.Text = "Powrót";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // buttonChangeStatus
            // 
            this.buttonChangeStatus.Location = new System.Drawing.Point(601, 216);
            this.buttonChangeStatus.Name = "buttonChangeStatus";
            this.buttonChangeStatus.Size = new System.Drawing.Size(140, 28);
            this.buttonChangeStatus.TabIndex = 15;
            this.buttonChangeStatus.Text = "Zmień status";
            this.buttonChangeStatus.UseVisualStyleBackColor = true;
            this.buttonChangeStatus.Click += new System.EventHandler(this.buttonChangeStatus_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Location = new System.Drawing.Point(601, 54);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(140, 28);
            this.buttonEdit.TabIndex = 16;
            this.buttonEdit.Text = "Edytuj";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonAcceptEdit
            // 
            this.buttonAcceptEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAcceptEdit.ForeColor = System.Drawing.Color.Blue;
            this.buttonAcceptEdit.Location = new System.Drawing.Point(601, 114);
            this.buttonAcceptEdit.Name = "buttonAcceptEdit";
            this.buttonAcceptEdit.Size = new System.Drawing.Size(140, 28);
            this.buttonAcceptEdit.TabIndex = 17;
            this.buttonAcceptEdit.Text = "Akceptuj edycje";
            this.buttonAcceptEdit.UseVisualStyleBackColor = true;
            this.buttonAcceptEdit.Click += new System.EventHandler(this.buttonAcceptEdit_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(601, 260);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 18;
            // 
            // buttonAcceptStatusChanged
            // 
            this.buttonAcceptStatusChanged.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAcceptStatusChanged.ForeColor = System.Drawing.Color.Blue;
            this.buttonAcceptStatusChanged.Location = new System.Drawing.Point(601, 293);
            this.buttonAcceptStatusChanged.Name = "buttonAcceptStatusChanged";
            this.buttonAcceptStatusChanged.Size = new System.Drawing.Size(140, 43);
            this.buttonAcceptStatusChanged.TabIndex = 19;
            this.buttonAcceptStatusChanged.Text = "Akceptuj zmiane statusu";
            this.buttonAcceptStatusChanged.UseVisualStyleBackColor = true;
            this.buttonAcceptStatusChanged.Click += new System.EventHandler(this.buttonAcceptStatusChanged_Click);
            // 
            // labelSelectStatus
            // 
            this.labelSelectStatus.AutoSize = true;
            this.labelSelectStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSelectStatus.Location = new System.Drawing.Point(607, 216);
            this.labelSelectStatus.Name = "labelSelectStatus";
            this.labelSelectStatus.Size = new System.Drawing.Size(130, 24);
            this.labelSelectStatus.TabIndex = 20;
            this.labelSelectStatus.Text = "Wybierz status";
            // 
            // ReservationDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelSelectStatus);
            this.Controls.Add(this.buttonAcceptStatusChanged);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.buttonAcceptEdit);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonChangeStatus);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.numericUpDownVal);
            this.Controls.Add(this.dateTimePickerDateEnd);
            this.Controls.Add(this.dateTimePickerDateZak);
            this.Controls.Add(this.dateTimePickerDateRez);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.textBoxNrRoom);
            this.Controls.Add(this.textBoxNrRez);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ReservationDetails";
            this.Text = "Szczegóły";
            this.Load += new System.EventHandler(this.ReservationDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxNrRez;
        private System.Windows.Forms.TextBox textBoxNrRoom;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateRez;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateZak;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateEnd;
        private System.Windows.Forms.NumericUpDown numericUpDownVal;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Button buttonChangeStatus;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonAcceptEdit;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonAcceptStatusChanged;
        private System.Windows.Forms.Label labelSelectStatus;
    }
}