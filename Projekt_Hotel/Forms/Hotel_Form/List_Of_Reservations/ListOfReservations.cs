﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Reservations.Reservation_Details;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Reservations
{
    public partial class ListOfReservations : Form
    {
        public ListOfReservations()
        {
            InitializeComponent();
        }

        private void ListOfReservations_Load(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var reservations = context.Rezerwacja_pokoi.ToList();
            listView1.Items.Clear();
            foreach (var x in reservations)
            {
                ListViewItem item = new ListViewItem(x.Rezerwacja_pokoju_id.ToString());
                item.SubItems.Add(x.Wartosc.ToString());
                item.SubItems.Add(x.Pokoj_id.ToString());
                item.SubItems.Add(Enum.GetName(typeof(EnumClass.Status),x.Status_id));
                listView1.Items.Add(item);
            }
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormState.goToHotelForm.Show();
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                var tmp = Convert.ToInt32(item.SubItems[0].Text);
                ReservationDetails rd = new ReservationDetails(tmp);
                this.Hide();
                FormState.PreviousPage = this;
                rd.Closed += (s, args) => this.Close();
                rd.Show();
            }
        }
    }
}
