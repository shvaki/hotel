﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.New_reservation
{
    public partial class NewReservation : Form
    {
        public NewReservation()
        {
            InitializeComponent();
        }

        private void NewReservation_Load(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();

            dateTimePickerRez.Format = DateTimePickerFormat.Custom;
            dateTimePickerRez.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerZak.Format = DateTimePickerFormat.Custom;
            dateTimePickerZak.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            dateTimePickerEnd.Format = DateTimePickerFormat.Custom;
            dateTimePickerEnd.CustomFormat = "yyyy/MM/dd hh:mm:ss";
            textBoxPhoneNr.Mask = "000-000-000";

            var rooms = context.Pokoj.ToList();
            var reservations = context.Rezerwacja_pokoi.ToList();
            
            var numbersOfFreeRooms = new List<Tuple<int, int>>();
            foreach (var x in rooms)
            {

                    if(x.Status_id==5)
                    {
                        numbersOfFreeRooms.Add(new Tuple<int, int>(x.Pokoj_id, x.Ilu_osobowy));
                    }
                
            }
          
            comboBox1.Items.AddRange(numbersOfFreeRooms.ToArray());
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            //validation
            if (textBoxName.Text.Length <= 0 || textBoxName.Text.Length >= 50)
            {
                labelErrorName.Text = "Nieprawdiłowe imie";
                labelErrorName.Show();
            }
            else if (textBoxSurname.Text.Length <= 0 || textBoxSurname.Text.Length >= 50)
            {
                labelErrorSurname.Text = "Nieprawdiłowe nazwisko";
                labelErrorSurname.Show();
            }
            else if ((textBoxAddress.Text == null || textBoxAddress.Text.Length <= 0) && textBoxAddress.Enabled==true)
            {
                labelErrorAddress.Text = "Nieprawdiłowy adres";
                labelErrorAddress.Show();
            }
            else if ((textBoxPhoneNr.Text.Length <= 0 || textBoxPhoneNr.Text.Length > 11) && textBoxPhoneNr.Enabled==true )
            {
                labelErrorPhoneNr.Text = "Nieprawdiłowy numer telefonu";
                labelErrorPhoneNr.Show();
            }
            else
            {
                //success
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var reservations = context.Rezerwacja_pokoi.ToList();
                var clients = context.Klient.ToList();
               
                Boolean addedReservation = false;


                foreach (var x in clients)
                {
                    if (x.Imie.Equals(textBoxName.Text) && x.Nazwisko.Equals(textBoxSurname.Text))
                    {
                        var numberOfRoom = comboBox1.SelectedItem.ToString().Split(',');
                        var idRoom = numberOfRoom[0].Substring(0 + 1);
                        int idRoom_inInt = Int32.Parse(idRoom);
                        var room = context.Pokoj.Where(i => i.Pokoj_id == idRoom_inInt).FirstOrDefault();

                        var day_diff = dateTimePickerEnd.Value - dateTimePickerZak.Value;

                        var payment = room.Wartosc * day_diff.Days;
                        double? realPayment=0;
                        if (x.Ilosc_wizyt_hotel > 0)
                            realPayment = payment / (x.Ilosc_wizyt_hotel * 0.01);
                        else
                            realPayment = payment;
                        var tmp = Convert.ToDouble(realPayment);

                        context.Rezerwacja_pokoi.Add(new Rezerwacja_pokoi(
                            reservations.Count+1,
                            tmp,
                            dateTimePickerRez.Value,
                            dateTimePickerZak.Value,
                            dateTimePickerEnd.Value,
                            2,//zarezerwowano
                            x.Klient_id,
                            room.Pokoj_id
                            ));
                        room.Status_id = Convert.ToInt32(EnumClass.Status.Zajety);
                        context.SaveChanges();
                        TimerForHotelReser timer = new TimerForHotelReser(reservations.Count + 1);
                        timer.startTimer();
                        addedReservation = true;
                        FormState.goToHotelForm.Show();
                        this.Hide();
                    }
                }
                if (!addedReservation)
                {
                    var Dk = context.Dane_Kontaktowe.ToList();
                    context.Dane_Kontaktowe.Add(new Dane_Kontaktowe(
                        Dk.Count + 1,
                        textBoxAddress.Text,
                        textBoxPhoneNr.Text
                        ));
                    context.Klient.Add(new Klient(
                        clients.Count+1,
                        textBoxName.Text,
                        textBoxSurname.Text,
                        Dk.Count+1,
                        1//hotel
                        ));
                    context.SaveChanges();
                    clients = context.Klient.ToList();
                    var numberOfRoom = comboBox1.SelectedItem.ToString().Split(',');
                    var idRoom = numberOfRoom[0].Substring(0 + 1);
                    int idRoom_inInt = Int32.Parse(idRoom);
                    var room = context.Pokoj.Where(i => i.Pokoj_id == idRoom_inInt).FirstOrDefault();

                    var day_diff = dateTimePickerEnd.Value - dateTimePickerZak.Value;

                    var payment = room.Wartosc * day_diff.Days;
                    double? realPayment = 0;
                    if (clients[clients.Count-1].Ilosc_wizyt_hotel > 0)
                        realPayment = payment / (clients[clients.Count-1].Ilosc_wizyt_hotel * 0.01);
                    else
                        realPayment = payment;
                    var tmp = Convert.ToDouble(realPayment);
                    context.Rezerwacja_pokoi.Add(new Rezerwacja_pokoi(
                        reservations.Count + 1,
                        tmp,
                        dateTimePickerRez.Value,
                        dateTimePickerZak.Value,
                        dateTimePickerEnd.Value,
                        2,//zarezerwowoano
                        clients[clients.Count-1].Klient_id,
                        room.Pokoj_id
                        ));
                    room.Status_id = Convert.ToInt32(EnumClass.Status.Zajety);
                    context.SaveChanges();
                    TimerForHotelReser timer = new TimerForHotelReser(reservations.Count + 1);
                    timer.startTimer();
                    FormState.goToHotelForm.Show();
                    this.Hide();
                }
            }



        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            FormState.goToHotelForm.Show();
            this.Hide();
        }

        private void textBoxSurname_TextChanged(object sender, EventArgs e)
        {
            if (textBoxName.Text.Length > 0)
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getGuy = context.Klient.Where(n=>n.Nazwisko.Equals(textBoxSurname.Text) && n.Imie.Equals(textBoxName.Text) && n.Identity_id==1).FirstOrDefault();
                if (getGuy != null)
                {
                    textBoxAddress.Hide();
                    textBoxPhoneNr.Hide();
                    textBoxAddress.Enabled = false;
                    textBoxPhoneNr.Enabled = false;
                }
                else
                {
                    textBoxAddress.Show();
                    textBoxPhoneNr.Show();
                    textBoxAddress.Enabled = true;
                    textBoxPhoneNr.Enabled = true;
                }
            }
        }
    }
}
