﻿namespace Projekt_Hotel.Forms.Hotel_Form.New_reservation
{
    partial class NewReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelDateRes = new System.Windows.Forms.Label();
            this.labelDateZak = new System.Windows.Forms.Label();
            this.labelDataEnd = new System.Windows.Forms.Label();
            this.labelRoom = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.dateTimePickerRez = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerZak = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonAccept = new System.Windows.Forms.Button();
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.labelErrorName = new System.Windows.Forms.Label();
            this.labelErrorSurname = new System.Windows.Forms.Label();
            this.labelPhoneNr = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.labelErrorPhoneNr = new System.Windows.Forms.Label();
            this.labelErrorAddress = new System.Windows.Forms.Label();
            this.textBoxPhoneNr = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(28, 54);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(50, 24);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Imie:";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSurname.Location = new System.Drawing.Point(28, 104);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(95, 24);
            this.labelSurname.TabIndex = 1;
            this.labelSurname.Text = "Nazwisko:";
            // 
            // labelDateRes
            // 
            this.labelDateRes.AutoSize = true;
            this.labelDateRes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateRes.Location = new System.Drawing.Point(28, 262);
            this.labelDateRes.Name = "labelDateRes";
            this.labelDateRes.Size = new System.Drawing.Size(142, 24);
            this.labelDateRes.TabIndex = 2;
            this.labelDateRes.Text = "Data rezerwacji:";
            // 
            // labelDateZak
            // 
            this.labelDateZak.AutoSize = true;
            this.labelDateZak.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateZak.Location = new System.Drawing.Point(28, 303);
            this.labelDateZak.Name = "labelDateZak";
            this.labelDateZak.Size = new System.Drawing.Size(190, 24);
            this.labelDateZak.TabIndex = 3;
            this.labelDateZak.Text = "Data zakwaterowania:";
            // 
            // labelDataEnd
            // 
            this.labelDataEnd.AutoSize = true;
            this.labelDataEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDataEnd.Location = new System.Drawing.Point(28, 345);
            this.labelDataEnd.Name = "labelDataEnd";
            this.labelDataEnd.Size = new System.Drawing.Size(170, 24);
            this.labelDataEnd.TabIndex = 4;
            this.labelDataEnd.Text = "Data końca pobytu:";
            // 
            // labelRoom
            // 
            this.labelRoom.AutoSize = true;
            this.labelRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRoom.Location = new System.Drawing.Point(522, 38);
            this.labelRoom.Name = "labelRoom";
            this.labelRoom.Size = new System.Drawing.Size(132, 24);
            this.labelRoom.TabIndex = 5;
            this.labelRoom.Text = "Wybór pokoju:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(239, 59);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(203, 20);
            this.textBoxName.TabIndex = 6;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(239, 109);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(203, 20);
            this.textBoxSurname.TabIndex = 7;
            this.textBoxSurname.TextChanged += new System.EventHandler(this.textBoxSurname_TextChanged);
            // 
            // dateTimePickerRez
            // 
            this.dateTimePickerRez.Location = new System.Drawing.Point(239, 264);
            this.dateTimePickerRez.Name = "dateTimePickerRez";
            this.dateTimePickerRez.Size = new System.Drawing.Size(203, 20);
            this.dateTimePickerRez.TabIndex = 8;
            // 
            // dateTimePickerZak
            // 
            this.dateTimePickerZak.Location = new System.Drawing.Point(239, 307);
            this.dateTimePickerZak.Name = "dateTimePickerZak";
            this.dateTimePickerZak.Size = new System.Drawing.Size(203, 20);
            this.dateTimePickerZak.TabIndex = 9;
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.Location = new System.Drawing.Point(239, 347);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(203, 20);
            this.dateTimePickerEnd.TabIndex = 10;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(526, 76);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(128, 21);
            this.comboBox1.TabIndex = 11;
            // 
            // buttonAccept
            // 
            this.buttonAccept.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonAccept.Location = new System.Drawing.Point(212, 401);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(211, 23);
            this.buttonAccept.TabIndex = 12;
            this.buttonAccept.Text = "Zatwierdź";
            this.buttonAccept.UseVisualStyleBackColor = false;
            this.buttonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.buttonGoBack.Location = new System.Drawing.Point(443, 401);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(211, 23);
            this.buttonGoBack.TabIndex = 13;
            this.buttonGoBack.Text = "Powrót";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // labelErrorName
            // 
            this.labelErrorName.AutoSize = true;
            this.labelErrorName.ForeColor = System.Drawing.Color.Red;
            this.labelErrorName.Location = new System.Drawing.Point(236, 38);
            this.labelErrorName.Name = "labelErrorName";
            this.labelErrorName.Size = new System.Drawing.Size(0, 13);
            this.labelErrorName.TabIndex = 14;
            // 
            // labelErrorSurname
            // 
            this.labelErrorSurname.AutoSize = true;
            this.labelErrorSurname.ForeColor = System.Drawing.Color.Red;
            this.labelErrorSurname.Location = new System.Drawing.Point(236, 93);
            this.labelErrorSurname.Name = "labelErrorSurname";
            this.labelErrorSurname.Size = new System.Drawing.Size(0, 13);
            this.labelErrorSurname.TabIndex = 15;
            // 
            // labelPhoneNr
            // 
            this.labelPhoneNr.AutoSize = true;
            this.labelPhoneNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPhoneNr.Location = new System.Drawing.Point(28, 153);
            this.labelPhoneNr.Name = "labelPhoneNr";
            this.labelPhoneNr.Size = new System.Drawing.Size(107, 24);
            this.labelPhoneNr.TabIndex = 16;
            this.labelPhoneNr.Text = "Nr telefonu:";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAddress.Location = new System.Drawing.Point(28, 193);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(65, 24);
            this.labelAddress.TabIndex = 17;
            this.labelAddress.Text = "Adres:";
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(239, 198);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(203, 20);
            this.textBoxAddress.TabIndex = 19;
            // 
            // labelErrorPhoneNr
            // 
            this.labelErrorPhoneNr.AutoSize = true;
            this.labelErrorPhoneNr.ForeColor = System.Drawing.Color.Red;
            this.labelErrorPhoneNr.Location = new System.Drawing.Point(236, 142);
            this.labelErrorPhoneNr.Name = "labelErrorPhoneNr";
            this.labelErrorPhoneNr.Size = new System.Drawing.Size(0, 13);
            this.labelErrorPhoneNr.TabIndex = 20;
            // 
            // labelErrorAddress
            // 
            this.labelErrorAddress.AutoSize = true;
            this.labelErrorAddress.ForeColor = System.Drawing.Color.Red;
            this.labelErrorAddress.Location = new System.Drawing.Point(236, 182);
            this.labelErrorAddress.Name = "labelErrorAddress";
            this.labelErrorAddress.Size = new System.Drawing.Size(0, 13);
            this.labelErrorAddress.TabIndex = 21;
            // 
            // textBoxPhoneNr
            // 
            this.textBoxPhoneNr.Location = new System.Drawing.Point(239, 158);
            this.textBoxPhoneNr.Name = "textBoxPhoneNr";
            this.textBoxPhoneNr.Size = new System.Drawing.Size(203, 20);
            this.textBoxPhoneNr.TabIndex = 22;
            // 
            // NewReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxPhoneNr);
            this.Controls.Add(this.labelErrorAddress);
            this.Controls.Add(this.labelErrorPhoneNr);
            this.Controls.Add(this.textBoxAddress);
            this.Controls.Add(this.labelAddress);
            this.Controls.Add(this.labelPhoneNr);
            this.Controls.Add(this.labelErrorSurname);
            this.Controls.Add(this.labelErrorName);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.buttonAccept);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.dateTimePickerEnd);
            this.Controls.Add(this.dateTimePickerZak);
            this.Controls.Add(this.dateTimePickerRez);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelRoom);
            this.Controls.Add(this.labelDataEnd);
            this.Controls.Add(this.labelDateZak);
            this.Controls.Add(this.labelDateRes);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.labelName);
            this.Name = "NewReservation";
            this.Text = "Dodaj rezerwacje";
            this.Load += new System.EventHandler(this.NewReservation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelDateRes;
        private System.Windows.Forms.Label labelDateZak;
        private System.Windows.Forms.Label labelDataEnd;
        private System.Windows.Forms.Label labelRoom;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.DateTimePicker dateTimePickerRez;
        private System.Windows.Forms.DateTimePicker dateTimePickerZak;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonAccept;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Label labelErrorName;
        private System.Windows.Forms.Label labelErrorSurname;
        private System.Windows.Forms.Label labelPhoneNr;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Label labelErrorPhoneNr;
        private System.Windows.Forms.Label labelErrorAddress;
        private System.Windows.Forms.MaskedTextBox textBoxPhoneNr;
    }
}