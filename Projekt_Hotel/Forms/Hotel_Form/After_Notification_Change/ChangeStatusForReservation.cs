﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.After_Notification_Change
{
    public partial class ChangeStatusForReservation : Form
    {
        public int id;
        public Boolean reservationEnd;
        public ChangeStatusForReservation(int uId, Boolean isReservationEnd)
        {
            this.id = uId;
            this.reservationEnd = isReservationEnd;
            InitializeComponent();
        }

        private void ChangeStatusForReservation_Load(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getReservation = context.Rezerwacja_pokoi.Where(i => i.Rezerwacja_pokoju_id == id).FirstOrDefault();
            var getClient = context.Klient.Where(i=>i.Klient_id==getReservation.Klient_id).FirstOrDefault();
            var getStatus = context.Status.ToList();

            labelName.Text = getClient.Nazwisko + " " + getClient.Imie;
            labelNrPokoju.Text = getReservation.Pokoj_id.ToString();
            labelStatus.Text = Enum.GetName(typeof(EnumClass.Status),getReservation.Status_id);
            labelDateEnd.Text = getReservation.Data_zakonczenia.ToString();
            labelDateZak.Text = getReservation.Data_zakwaterowania.ToString();

           
            foreach (var x in getStatus)
            {
                if ((x.IdStatusu == 1 || x.IdStatusu == 10) && !reservationEnd)
                {
                    comboBox1.Items.Add(x.Nazwa);
                }
                else if ((x.IdStatusu == 3 || x.IdStatusu == 10 || x.IdStatusu == 4) && reservationEnd ) 
                {
                    comboBox1.Items.Add(x.Nazwa);
                }
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var getReservation = context.Rezerwacja_pokoi.Where(i => i.Rezerwacja_pokoju_id == id).FirstOrDefault();
                var tmp = (int)Enum.Parse(typeof(EnumClass.Status), comboBox1.SelectedItem.ToString());

                if (tmp >= 3 && tmp <= 4)
                {
                    var getRoom = context.Pokoj.Where(i => i.Pokoj_id == getReservation.Pokoj_id).FirstOrDefault();
                    getRoom.Status_id = 5;
                    getReservation.Status_id = tmp;
                    context.SaveChanges();
                }
                else
                {
                    getReservation.Status_id = tmp;
                    context.SaveChanges();
                }

                
                this.Hide();
               


            }
        }
    }
}
