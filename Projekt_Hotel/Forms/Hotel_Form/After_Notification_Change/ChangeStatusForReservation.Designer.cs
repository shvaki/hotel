﻿namespace Projekt_Hotel.Forms.Hotel_Form.After_Notification_Change
{
    partial class ChangeStatusForReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.labelNrPokoju = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelDateZak = new System.Windows.Forms.Label();
            this.labelDateEnd = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelChangeStat = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(25, 37);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(57, 20);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "label1";
            // 
            // labelNrPokoju
            // 
            this.labelNrPokoju.AutoSize = true;
            this.labelNrPokoju.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNrPokoju.Location = new System.Drawing.Point(25, 93);
            this.labelNrPokoju.Name = "labelNrPokoju";
            this.labelNrPokoju.Size = new System.Drawing.Size(57, 20);
            this.labelNrPokoju.TabIndex = 1;
            this.labelNrPokoju.Text = "label1";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatus.Location = new System.Drawing.Point(25, 154);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(57, 20);
            this.labelStatus.TabIndex = 2;
            this.labelStatus.Text = "label1";
            // 
            // labelDateZak
            // 
            this.labelDateZak.AutoSize = true;
            this.labelDateZak.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateZak.Location = new System.Drawing.Point(25, 213);
            this.labelDateZak.Name = "labelDateZak";
            this.labelDateZak.Size = new System.Drawing.Size(57, 20);
            this.labelDateZak.TabIndex = 3;
            this.labelDateZak.Text = "label1";
            // 
            // labelDateEnd
            // 
            this.labelDateEnd.AutoSize = true;
            this.labelDateEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateEnd.Location = new System.Drawing.Point(25, 269);
            this.labelDateEnd.Name = "labelDateEnd";
            this.labelDateEnd.Size = new System.Drawing.Size(57, 20);
            this.labelDateEnd.TabIndex = 4;
            this.labelDateEnd.Text = "label1";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(426, 56);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(137, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // labelChangeStat
            // 
            this.labelChangeStat.AutoSize = true;
            this.labelChangeStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelChangeStat.Location = new System.Drawing.Point(437, 22);
            this.labelChangeStat.Name = "labelChangeStat";
            this.labelChangeStat.Size = new System.Drawing.Size(113, 20);
            this.labelChangeStat.TabIndex = 6;
            this.labelChangeStat.Text = "Zmień status";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.Location = new System.Drawing.Point(426, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 34);
            this.button1.TabIndex = 7;
            this.button1.Text = "Potwierdź";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ChangeStatusForReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelChangeStat);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelDateEnd);
            this.Controls.Add(this.labelDateZak);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelNrPokoju);
            this.Controls.Add(this.labelName);
            this.Name = "ChangeStatusForReservation";
            this.Text = "Zmień status rezerwacji";
            this.Load += new System.EventHandler(this.ChangeStatusForReservation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelNrPokoju;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelDateZak;
        private System.Windows.Forms.Label labelDateEnd;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label labelChangeStat;
        private System.Windows.Forms.Button button1;
    }
}