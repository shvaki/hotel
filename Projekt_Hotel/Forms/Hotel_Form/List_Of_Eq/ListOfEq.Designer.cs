﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq
{
    partial class ListOfEq
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeaderid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderVal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.buttonAddEq = new System.Windows.Forms.Button();
            this.buttonAddSet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderid,
            this.columnHeaderName,
            this.columnHeaderVal,
            this.columnHeaderStat});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(27, 24);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(286, 380);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // columnHeaderid
            // 
            this.columnHeaderid.Text = "L.P";
            this.columnHeaderid.Width = 28;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Nazwa";
            this.columnHeaderName.Width = 73;
            // 
            // columnHeaderVal
            // 
            this.columnHeaderVal.Text = "Cena";
            this.columnHeaderVal.Width = 72;
            // 
            // columnHeaderStat
            // 
            this.columnHeaderStat.Text = "Status";
            this.columnHeaderStat.Width = 108;
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.buttonGoBack.Location = new System.Drawing.Point(373, 381);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(115, 23);
            this.buttonGoBack.TabIndex = 1;
            this.buttonGoBack.Text = "Powrót";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // buttonAddEq
            // 
            this.buttonAddEq.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonAddEq.Location = new System.Drawing.Point(373, 24);
            this.buttonAddEq.Name = "buttonAddEq";
            this.buttonAddEq.Size = new System.Drawing.Size(115, 23);
            this.buttonAddEq.TabIndex = 2;
            this.buttonAddEq.Text = "Dodaj sprzęt";
            this.buttonAddEq.UseVisualStyleBackColor = false;
            this.buttonAddEq.Click += new System.EventHandler(this.buttonAddEq_Click);
            // 
            // buttonAddSet
            // 
            this.buttonAddSet.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonAddSet.Location = new System.Drawing.Point(373, 88);
            this.buttonAddSet.Name = "buttonAddSet";
            this.buttonAddSet.Size = new System.Drawing.Size(115, 23);
            this.buttonAddSet.TabIndex = 3;
            this.buttonAddSet.Text = "Dodaj zestaw";
            this.buttonAddSet.UseVisualStyleBackColor = false;
            this.buttonAddSet.Click += new System.EventHandler(this.buttonAddSet_Click);
            // 
            // ListOfEq
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(544, 463);
            this.Controls.Add(this.buttonAddSet);
            this.Controls.Add(this.buttonAddEq);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.listView1);
            this.Name = "ListOfEq";
            this.Text = "Lista wyposażenia";
            this.Load += new System.EventHandler(this.ListOfEq_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeaderid;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderVal;
        private System.Windows.Forms.ColumnHeader columnHeaderStat;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Button buttonAddEq;
        private System.Windows.Forms.Button buttonAddSet;
    }
}