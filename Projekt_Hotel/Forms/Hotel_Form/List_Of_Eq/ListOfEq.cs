﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Add_Eq;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Add_Set;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Details_Eq;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq
{
    public partial class ListOfEq : Form
    {
        public ListOfEq()
        {
            InitializeComponent();
            listView1.Items.Clear();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var eq = context.Sprzet.ToList();
            foreach (var x in eq)
            {
                if (x.Wyposazenie_id == 1) {
                    ListViewItem item = new ListViewItem(x.IdSprzetu.ToString());
                    item.SubItems.Add(x.Nazwa);
                    item.SubItems.Add(x.Cena.ToString());
                    String y = Enum.GetName(typeof(EnumClass.Status), x.Status_id);
                    item.SubItems.Add(y);
                    listView1.Items.Add(item);
                }

            }
        }

        private void buttonAddEq_Click(object sender, EventArgs e)
        {
            AddEq addeq = new AddEq();
            this.Hide();
            addeq.Show();
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormState.goToHotelForm.Show();
        }

        private void ListOfEq_Load(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var eq = context.Sprzet.ToList();
            foreach (var x in eq)
            {
                ListViewItem item = new ListViewItem(x.IdSprzetu.ToString());
                item.SubItems.Add(x.Nazwa);
                item.SubItems.Add(x.Cena.ToString());
                String y = Enum.GetName(typeof(EnumClass.Status), x.Status_id);
                item.SubItems.Add(y);
                listView1.Items.Add(item);
            }
            if (FormState.NameOfLoggedPerson != "admin")
            {
                buttonAddEq.Hide();
                buttonAddSet.Hide();
            }
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {

            if (listView1.SelectedItems.Count > 0) {
                ListViewItem item = listView1.SelectedItems[0];
               
                var tmp = Convert.ToInt32(item.SubItems[0].Text);
                DetailsEq dq = new DetailsEq(tmp);
                this.Hide();
                dq.Show();
            }

        }

        private void buttonAddSet_Click(object sender, EventArgs e)
        {
            AddSet addset = new AddSet();
            this.Hide();
            addset.Show();
        }
    }
}
