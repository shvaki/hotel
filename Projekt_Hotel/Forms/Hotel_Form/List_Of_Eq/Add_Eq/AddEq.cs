﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Add_Eq
{
    public partial class AddEq : Form
    {
        public AddEq()
        {
            InitializeComponent();
        }

        private void AddEq_Load(object sender, EventArgs e)
        {

        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            ListOfEq eq = new ListOfEq();
            this.Hide();
            eq.Show();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 0 || textBox1.Text.Length > 50)
            {
                labelErrorName.Text = "Nieprawidłowa nazwa";
            }
            else
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var lengthOfEq = context.Sprzet.ToList();
               

                var tmpForDb = Convert.ToDouble(numericUpDown1.Value);
                int tmpForEnum = Convert.ToInt32(EnumClass.Status.Dziala);
                context.Sprzet.Add(new Sprzet(
                    lengthOfEq[lengthOfEq.Count - 1].IdSprzetu + 1,
                    textBox1.Text,
                    tmpForDb,
                    tmpForEnum,
                    1//zawsze 1 bo to default 
                    ));
                context.SaveChanges();
                ListOfEq eq = new ListOfEq();
                this.Hide();
                eq.Show();
            }
        }
    }
}
