﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Add_Set
{
    public partial class AddSet : Form
    {
        public AddSet()
        {
            InitializeComponent();
        }

        private void buttonToNextList_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                List<String> names = checkedListBox1.CheckedItems.OfType<String>().ToList();
                foreach (var x in names)
                {
                    listView1.Items.Add(x);
                }
                for (int i = checkedListBox1.Items.Count - 1; i >= 0; i--)
                {
                    
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        checkedListBox1.Items.Remove(checkedListBox1.Items[i]);
                    }
                }
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count > 0 && textBoxNameSet.Text.Length>0 && textBoxNameSet.Text.Length <50)
            {
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var setsLength = context.Wyposazenie_pokoju.ToList();
                var tmp = setsLength[setsLength.Count - 1].Wyposazenie_id + 1;
                var ifExist = context.Wyposazenie_pokoju.Where(n => n.Nazwa.Equals(textBoxNameSet.Text)).FirstOrDefault();
                if (ifExist == null)
                {
                    context.Wyposazenie_pokoju.Add(new Wyposazenie_pokoju(
                        tmp,
                        textBoxNameSet.Text
                        ));
                    context.SaveChanges();
                }
                else
                    tmp = ifExist.Wyposazenie_id;

                
                foreach (ListViewItem x in listView1.Items)
                {
                    var findEq = context.Sprzet.Where(n => n.Nazwa.Equals(x.Text) && n.Wyposazenie_id==1).FirstOrDefault();
                    findEq.Wyposazenie_id = tmp;
                    context.SaveChanges();
                }
            }
            ListOfEq le = new ListOfEq();
            this.Hide();
            le.Show();
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            ListOfEq le = new ListOfEq();
            this.Hide();
            le.Show();
        }

        private void AddSet_Load(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            listView1.Items.Clear();
            checkedListBox1.Items.Clear();
            var eq = context.Sprzet.ToList();

            List<String> names = new List<String>();
            foreach (var x in eq)
            {
                if (x.Wyposazenie_id == 1)
                {
                    names.Add(x.Nazwa);
                }
            }
            checkedListBox1.Items.AddRange(names.ToArray());
        }
    }
}
