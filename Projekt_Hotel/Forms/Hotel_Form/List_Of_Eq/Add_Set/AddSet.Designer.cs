﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Add_Set
{
    partial class AddSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNameSet = new System.Windows.Forms.TextBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.buttonToNextList = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(12, 12);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(217, 409);
            this.checkedListBox1.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(298, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(208, 409);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(573, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nazwa zestawu";
            // 
            // textBoxNameSet
            // 
            this.textBoxNameSet.Location = new System.Drawing.Point(578, 77);
            this.textBoxNameSet.Name = "textBoxNameSet";
            this.textBoxNameSet.Size = new System.Drawing.Size(157, 20);
            this.textBoxNameSet.TabIndex = 3;
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonAdd.Location = new System.Drawing.Point(578, 345);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(157, 23);
            this.buttonAdd.TabIndex = 4;
            this.buttonAdd.Text = "Dodaj";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.buttonGoBack.Location = new System.Drawing.Point(578, 389);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(157, 23);
            this.buttonGoBack.TabIndex = 5;
            this.buttonGoBack.Text = "Powrót";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // buttonToNextList
            // 
            this.buttonToNextList.Location = new System.Drawing.Point(236, 137);
            this.buttonToNextList.Name = "buttonToNextList";
            this.buttonToNextList.Size = new System.Drawing.Size(56, 70);
            this.buttonToNextList.TabIndex = 6;
            this.buttonToNextList.Text = ">";
            this.buttonToNextList.UseVisualStyleBackColor = true;
            this.buttonToNextList.Click += new System.EventHandler(this.buttonToNextList_Click);
            // 
            // AddSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonToNextList);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.textBoxNameSet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.checkedListBox1);
            this.Name = "AddSet";
            this.Text = "Dodaj zestaw";
            this.Load += new System.EventHandler(this.AddSet_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNameSet;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Button buttonToNextList;
    }
}