﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Details_Eq
{
    partial class DetailsEq
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxVal = new System.Windows.Forms.TextBox();
            this.textBoxStat = new System.Windows.Forms.TextBox();
            this.textBoxSet = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonChangeStat = new System.Windows.Forms.Button();
            this.buttonRepaird = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(26, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nazwa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(26, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cena:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(26, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Status:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(26, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Zestaw:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(155, 48);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.ReadOnly = true;
            this.textBoxName.Size = new System.Drawing.Size(188, 20);
            this.textBoxName.TabIndex = 4;
            // 
            // textBoxVal
            // 
            this.textBoxVal.Location = new System.Drawing.Point(155, 106);
            this.textBoxVal.Name = "textBoxVal";
            this.textBoxVal.ReadOnly = true;
            this.textBoxVal.Size = new System.Drawing.Size(188, 20);
            this.textBoxVal.TabIndex = 5;
            // 
            // textBoxStat
            // 
            this.textBoxStat.Location = new System.Drawing.Point(155, 164);
            this.textBoxStat.Name = "textBoxStat";
            this.textBoxStat.ReadOnly = true;
            this.textBoxStat.Size = new System.Drawing.Size(188, 20);
            this.textBoxStat.TabIndex = 6;
            // 
            // textBoxSet
            // 
            this.textBoxSet.Location = new System.Drawing.Point(155, 223);
            this.textBoxSet.Name = "textBoxSet";
            this.textBoxSet.ReadOnly = true;
            this.textBoxSet.Size = new System.Drawing.Size(188, 20);
            this.textBoxSet.TabIndex = 7;
            // 
            // buttonOK
            // 
            this.buttonOK.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonOK.Location = new System.Drawing.Point(15, 327);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(125, 49);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = false;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonChangeStat
            // 
            this.buttonChangeStat.BackColor = System.Drawing.Color.PaleVioletRed;
            this.buttonChangeStat.Location = new System.Drawing.Point(155, 327);
            this.buttonChangeStat.Name = "buttonChangeStat";
            this.buttonChangeStat.Size = new System.Drawing.Size(125, 49);
            this.buttonChangeStat.TabIndex = 9;
            this.buttonChangeStat.Text = "Zgłoś usterke";
            this.buttonChangeStat.UseVisualStyleBackColor = false;
            this.buttonChangeStat.Click += new System.EventHandler(this.buttonChangeStat_Click);
            // 
            // buttonRepaird
            // 
            this.buttonRepaird.BackColor = System.Drawing.Color.DarkGray;
            this.buttonRepaird.Location = new System.Drawing.Point(293, 327);
            this.buttonRepaird.Name = "buttonRepaird";
            this.buttonRepaird.Size = new System.Drawing.Size(125, 49);
            this.buttonRepaird.TabIndex = 10;
            this.buttonRepaird.Text = "Naprawiono";
            this.buttonRepaird.UseVisualStyleBackColor = false;
            this.buttonRepaird.Click += new System.EventHandler(this.buttonRepaird_Click);
            // 
            // DetailsEq
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(430, 429);
            this.Controls.Add(this.buttonRepaird);
            this.Controls.Add(this.buttonChangeStat);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxSet);
            this.Controls.Add(this.textBoxStat);
            this.Controls.Add(this.textBoxVal);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DetailsEq";
            this.Text = "Szczegóły sprzętu";
            this.Load += new System.EventHandler(this.DetailsEq_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxVal;
        private System.Windows.Forms.TextBox textBoxStat;
        private System.Windows.Forms.TextBox textBoxSet;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonChangeStat;
        private System.Windows.Forms.Button buttonRepaird;
    }
}