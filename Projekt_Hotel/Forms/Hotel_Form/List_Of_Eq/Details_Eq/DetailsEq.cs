﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq.Details_Eq
{
    public partial class DetailsEq : Form
    {
        public int id;
        public DetailsEq(int uid)
        {
            this.id = uid;
            InitializeComponent();
        }

        private void DetailsEq_Load(object sender, EventArgs e)
        {
            if (FormState.NameOfLoggedPerson != "admin")
                buttonRepaird.Hide();
                  
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var eqStuff = context.Sprzet.Where(i => i.IdSprzetu == id).FirstOrDefault();
            var eq = context.Wyposazenie_pokoju.Where(i => i.Wyposazenie_id == eqStuff.Wyposazenie_id).FirstOrDefault();

            textBoxName.Text = eqStuff.Nazwa;
            textBoxVal.Text = eqStuff.Cena.ToString();
            textBoxStat.Text = Enum.GetName(typeof(EnumClass.Status), eqStuff.Status_id).ToString();
            textBoxSet.Text = eq.Nazwa;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            ListOfEq eq = new ListOfEq();
            this.Hide();
            eq.Show();
        }

        private void buttonChangeStat_Click(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var eqStuff = context.Sprzet.Where(i => i.IdSprzetu == id).FirstOrDefault();
            eqStuff.Status_id = Convert.ToInt32(EnumClass.Status.Naprawa);
            context.SaveChanges();

            ListOfEq eq = new ListOfEq();
            this.Hide();
            eq.Show();
        }

        private void buttonRepaird_Click(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var eqStuff = context.Sprzet.Where(i => i.IdSprzetu == id).FirstOrDefault();
            eqStuff.Status_id = Convert.ToInt32(EnumClass.Status.Dziala);
            context.SaveChanges();

            ListOfEq eq = new ListOfEq();
            this.Hide();
            eq.Show();
        }
    }
}
