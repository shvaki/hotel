﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms
{
    partial class ListOfRooms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddRoom = new System.Windows.Forms.Button();
            this.btnGoBack = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.NrRoom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PPL_room = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Value_room = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // buttonAddRoom
            // 
            this.buttonAddRoom.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonAddRoom.Location = new System.Drawing.Point(519, 41);
            this.buttonAddRoom.Name = "buttonAddRoom";
            this.buttonAddRoom.Size = new System.Drawing.Size(122, 23);
            this.buttonAddRoom.TabIndex = 1;
            this.buttonAddRoom.Text = "Dodaj pokój";
            this.buttonAddRoom.UseVisualStyleBackColor = false;
            this.buttonAddRoom.Click += new System.EventHandler(this.buttonAddRoom_Click);
            // 
            // btnGoBack
            // 
            this.btnGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.btnGoBack.Location = new System.Drawing.Point(519, 382);
            this.btnGoBack.Name = "btnGoBack";
            this.btnGoBack.Size = new System.Drawing.Size(122, 23);
            this.btnGoBack.TabIndex = 3;
            this.btnGoBack.Text = "Powrót";
            this.btnGoBack.UseVisualStyleBackColor = false;
            this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NrRoom,
            this.PPL_room,
            this.Value_room,
            this.columnHeaderStat});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(40, 41);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(399, 345);
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // NrRoom
            // 
            this.NrRoom.Text = "Nr";
            this.NrRoom.Width = 30;
            // 
            // PPL_room
            // 
            this.PPL_room.Text = "Ilu_osobowy";
            this.PPL_room.Width = 96;
            // 
            // Value_room
            // 
            this.Value_room.Text = "Cena na dobe";
            this.Value_room.Width = 108;
            // 
            // columnHeaderStat
            // 
            this.columnHeaderStat.Text = "Status";
            // 
            // ListOfRooms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(681, 440);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.btnGoBack);
            this.Controls.Add(this.buttonAddRoom);
            this.Name = "ListOfRooms";
            this.Text = "Lista pokoi";
            this.Load += new System.EventHandler(this.ListOfRooms_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonAddRoom;
        private System.Windows.Forms.Button btnGoBack;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader NrRoom;
        private System.Windows.Forms.ColumnHeader PPL_room;
        private System.Windows.Forms.ColumnHeader Value_room;
        private System.Windows.Forms.ColumnHeader columnHeaderStat;
    }
}