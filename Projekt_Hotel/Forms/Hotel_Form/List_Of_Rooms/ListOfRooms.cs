﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms.Add_Room;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms.Room_Details;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms
{
    public partial class ListOfRooms : Form
    {
        public ListOfRooms()
        {
            InitializeComponent();

            if (FormState.NameOfLoggedPerson != "admin")
                buttonAddRoom.Hide();
            listView1.Items.Clear();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var rooms = context.Pokoj.ToList();
            foreach (var x in rooms)
            {
                ListViewItem listViewItem = new ListViewItem(x.Pokoj_id.ToString());
                listViewItem.SubItems.Add(x.Ilu_osobowy.ToString());
                listViewItem.SubItems.Add(x.Wartosc.ToString());
                listViewItem.SubItems.Add(Enum.GetName(typeof(EnumClass.Status),x.Status_id));
                listView1.Items.Add(listViewItem);
            }
        }

        private void ListOfRooms_Load(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var rooms = context.Pokoj.ToList();
            foreach (var x in rooms)
            {

                ListViewItem listViewItem = new ListViewItem(x.Pokoj_id.ToString());
                listViewItem.SubItems.Add(x.Ilu_osobowy.ToString());
                listViewItem.SubItems.Add(x.Wartosc.ToString());
                listViewItem.SubItems.Add(Enum.GetName(typeof(EnumClass.Status), x.Status_id));
                listView1.Items.Add(listViewItem);
            }
        }

        private void buttonAddRoom_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddRoom rd = new AddRoom();
            FormState.PreviousPage = this;
            rd.Closed += (s, args) => this.Close();
            rd.Show();
        }


        private void btnGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormState.goToHotelForm.Show();
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var tmp = Convert.ToInt32(item.SubItems[0].Text);
                RoomDetails rd = new RoomDetails(tmp);
                this.Hide();
                FormState.PreviousPage = this;
               rd.Closed += (s, args) => this.Close();
                rd.Show();

            }
        }
    }
}
