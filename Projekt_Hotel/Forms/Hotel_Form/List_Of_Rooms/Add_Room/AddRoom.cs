﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms.Add_Room
{
    public partial class AddRoom : Form
    {
        public AddRoom()
        {
            InitializeComponent();
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getEq = context.Wyposazenie_pokoju.ToList();
            List<String> listOfNameEq = new List<String>();
            foreach (var x in getEq)
            {
                if(x.Nazwa != "DEFAULT")
                listOfNameEq.Add(x.Nazwa);
            }
            comboBox1.Items.AddRange(listOfNameEq.ToArray());
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormState.PreviousPage.Show();
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            if (numericUpDownPlaceForPPL.Value < 0 || numericUpDownPlaceForPPL.Value > 12)
            {
                labelErrorPPL.Text = "Nieprawidłowa liczba";
            }
            else if (numericUpDownValueForDay.Value < 0)
            {
                labelErrorValueForDay.Text = "Nieprawidłowa liczba";
            }
            else
            {
                //success
                Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
                var rooms = context.Pokoj.ToList();
                var eqs = context.Wyposazenie_pokoju.ToList();

                
                var tmpForPPL = Convert.ToInt32(numericUpDownPlaceForPPL.Value);
                var tmpForDayVal = Convert.ToDouble(numericUpDownValueForDay.Value);

                context.Pokoj.Add(new Pokoj(
                    rooms[rooms.Count-1].Pokoj_id+1,
                    eqs[eqs.Count-1].Wyposazenie_id,
                    tmpForPPL,
                    tmpForDayVal,
                    5//wolny

                    ));
                context.SaveChanges();
                this.Hide();
                ListOfRooms lr = new ListOfRooms();
                lr.Show();
            }
        }
    }
}
