﻿namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms.Add_Room
{
    partial class AddRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAccept = new System.Windows.Forms.Button();
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.numericUpDownPlaceForPPL = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownValueForDay = new System.Windows.Forms.NumericUpDown();
            this.labelErrorPPL = new System.Windows.Forms.Label();
            this.labelErrorValueForDay = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPlaceForPPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValueForDay)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ilu osobowy:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cena na dobe:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Wyposażenie:";
            // 
            // buttonAccept
            // 
            this.buttonAccept.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonAccept.Location = new System.Drawing.Point(139, 326);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(145, 35);
            this.buttonAccept.TabIndex = 8;
            this.buttonAccept.Text = "Zatwierdź";
            this.buttonAccept.UseVisualStyleBackColor = false;
            this.buttonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.BackColor = System.Drawing.Color.LightYellow;
            this.buttonGoBack.Location = new System.Drawing.Point(318, 326);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(145, 35);
            this.buttonGoBack.TabIndex = 9;
            this.buttonGoBack.Text = "Powrót";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // numericUpDownPlaceForPPL
            // 
            this.numericUpDownPlaceForPPL.Location = new System.Drawing.Point(242, 32);
            this.numericUpDownPlaceForPPL.Name = "numericUpDownPlaceForPPL";
            this.numericUpDownPlaceForPPL.Size = new System.Drawing.Size(221, 20);
            this.numericUpDownPlaceForPPL.TabIndex = 10;
            // 
            // numericUpDownValueForDay
            // 
            this.numericUpDownValueForDay.Location = new System.Drawing.Point(242, 90);
            this.numericUpDownValueForDay.Name = "numericUpDownValueForDay";
            this.numericUpDownValueForDay.Size = new System.Drawing.Size(221, 20);
            this.numericUpDownValueForDay.TabIndex = 11;
            // 
            // labelErrorPPL
            // 
            this.labelErrorPPL.AutoSize = true;
            this.labelErrorPPL.ForeColor = System.Drawing.Color.Red;
            this.labelErrorPPL.Location = new System.Drawing.Point(233, 13);
            this.labelErrorPPL.Name = "labelErrorPPL";
            this.labelErrorPPL.Size = new System.Drawing.Size(0, 13);
            this.labelErrorPPL.TabIndex = 13;
            // 
            // labelErrorValueForDay
            // 
            this.labelErrorValueForDay.AutoSize = true;
            this.labelErrorValueForDay.ForeColor = System.Drawing.Color.Red;
            this.labelErrorValueForDay.Location = new System.Drawing.Point(239, 72);
            this.labelErrorValueForDay.Name = "labelErrorValueForDay";
            this.labelErrorValueForDay.Size = new System.Drawing.Size(0, 13);
            this.labelErrorValueForDay.TabIndex = 14;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(242, 154);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(221, 21);
            this.comboBox1.TabIndex = 17;
            // 
            // AddRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(547, 440);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelErrorValueForDay);
            this.Controls.Add(this.labelErrorPPL);
            this.Controls.Add(this.numericUpDownValueForDay);
            this.Controls.Add(this.numericUpDownPlaceForPPL);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.buttonAccept);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddRoom";
            this.Text = "Dodaj pokój";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPlaceForPPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValueForDay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonAccept;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.NumericUpDown numericUpDownPlaceForPPL;
        private System.Windows.Forms.NumericUpDown numericUpDownValueForDay;
        private System.Windows.Forms.Label labelErrorPPL;
        private System.Windows.Forms.Label labelErrorValueForDay;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}