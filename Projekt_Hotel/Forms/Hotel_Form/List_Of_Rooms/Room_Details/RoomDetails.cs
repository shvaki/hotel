﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;

namespace Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms.Room_Details
{
    public partial class RoomDetails : Form
    {
        public int id;
        public RoomDetails(int uid)
        {
            this.id = uid;
            InitializeComponent();
            if (FormState.NameOfLoggedPerson != "admin")
                buttonDelete.Hide();

        }

        private void RoomDetails_Load(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var room = context.Pokoj.Where(i => i.Pokoj_id == id).FirstOrDefault();
            var eq = context.Wyposazenie_pokoju.Where(i => i.Wyposazenie_id == room.Wyposazenie_id).FirstOrDefault();
            var eqStuff = context.Sprzet.Where(i => i.Wyposazenie_id == eq.Wyposazenie_id).ToList();


            String eqSet = eq.Nazwa+"(";
            foreach (var x in eqStuff) {
                eqSet += x.Nazwa + ", ";
            }
            eqSet += ")";


            textBoxNr.Text = room.Pokoj_id.ToString();
            textBoxNrPlace.Text = room.Ilu_osobowy.ToString();
            textBoxValue.Text = room.Wartosc.ToString();
            textBoxEq.Text = eqSet;

            

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListOfRooms lr = new ListOfRooms();
            lr.Show();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var room = context.Pokoj.Where(i => i.Pokoj_id == id).FirstOrDefault();
            context.Pokoj.Remove(room);
            context.SaveChanges();
            this.Hide();
            ListOfRooms lr = new ListOfRooms();
            lr.Show();
        }
    }
}
