﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Clients;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Eq;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Reservations;
using Projekt_Hotel.Forms.Hotel_Form.List_Of_Rooms;
using Projekt_Hotel.Forms.Hotel_Form.New_reservation;

namespace Projekt_Hotel.Forms.Hotel_Form
{
    public partial class HotelForm : Form
    {
        public HotelForm()
        {
            InitializeComponent();
          
        }

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            FormState.goToStartUpForm.Show();
            this.Hide();
            FormState.PreviousPage = this;

        }

        private void btnListOfClients_Click(object sender, EventArgs e)
        {
            ListOfClients l = new ListOfClients();
            this.Hide();
            FormState.PreviousPage = this;
            l.Closed += (s, args) => this.Close();
            l.Show();
        }

        private void HotelForm_Load(object sender, EventArgs e)
        {
            labelLoggedPerson.Text = "Jestes zalogowany jako: " + FormState.NameOfLoggedPerson;
        }

        private void labelLoggedPerson_Click(object sender, EventArgs e)
        {

        }

        private void btnAddReservation_Click(object sender, EventArgs e)
        {
            NewReservation l = new NewReservation();
            this.Hide();
            FormState.PreviousPage = this;
            l.Closed += (s, args) => this.Close();
            l.Show();
        }

        private void btnListOfRooms_Click(object sender, EventArgs e)
        {
            ListOfRooms lr = new ListOfRooms();
            this.Hide();
            FormState.PreviousPage = this;
            lr.Closed += (s, args) => this.Close();
            lr.Show();
        }

        private void btnListOfEq_Click(object sender, EventArgs e)
        {
            ListOfEq le = new ListOfEq();
            this.Hide();
            FormState.PreviousPage = this;
            le.Closed += (s, args) => this.Close();
            le.Show();
        }

        private void btnListReservations_Click(object sender, EventArgs e)
        {
            ListOfReservations le = new ListOfReservations();
            this.Hide();
            FormState.PreviousPage = this;
            le.Closed += (s, args) => this.Close();
            le.Show();
        }
    }
}
