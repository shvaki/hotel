﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Classes;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form.After_Notification_Change;

namespace Projekt_Hotel.Forms.Hotel_Form.TimerNotification
{
    public partial class Powiadomienie : Form
    {
        public int id;
        public Boolean reservationEnd;
        public Powiadomienie(int uId,Boolean isReservationEnd)
        {
            this.id = uId;
            this.reservationEnd = isReservationEnd;
            InitializeComponent();
        }

        private void Powiadomienie_Load(object sender, EventArgs e)
        {
            if (reservationEnd)
            {
                labelMessage.Text = "Upłynął czas zakwaterowania, zmień jej status klikając na dzwonek";
                Left = Screen.PrimaryScreen.Bounds.Width - Width - 20;
            }
            else
            {
                labelMessage.Text = "Upłynął czas rezerwacji, zmień jej status klikając na dzwonek";
                Left = Screen.PrimaryScreen.Bounds.Width - Width - 20;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ChangeStatusForReservation ch = new ChangeStatusForReservation(id,reservationEnd);
            ch.Show();
            this.Hide();
        }
    }
}
