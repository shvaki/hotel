﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Database;
using Projekt_Hotel.Forms.Hotel_Form.TimerNotification;

namespace Projekt_Hotel.Classes
{
    class TimerForHotelReser
    {
        public  int idRes;
        public static int idForMethods;
        private static Timer rezToZakTimer;
        private static Timer zakToEndTimer;
        public TimerForHotelReser(int uId)
        {
            this.idRes = uId;
            changeId(uId);
        }
        public void startTimer() {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            DateTime localDate = DateTime.Now;
            var getReservation = context.Rezerwacja_pokoi.Where(i=>i.Rezerwacja_pokoju_id==idRes).FirstOrDefault();
            var timeFromRezToZak = getReservation.Data_zakwaterowania - localDate;
            var timeFromZakToEnd = getReservation.Data_zakonczenia - localDate;

            if (timeFromRezToZak.TotalMilliseconds > 0)
            {
                rezToZakTimer = new Timer();
                rezToZakTimer.Interval = Convert.ToInt32(timeFromRezToZak.TotalMilliseconds);
                rezToZakTimer.Tick += new EventHandler(TimerEventProcessorZak);
                rezToZakTimer.Start();
            }

            if (timeFromZakToEnd.TotalMilliseconds > 0)
            {
                zakToEndTimer = new Timer();
                zakToEndTimer.Interval = Convert.ToInt32(timeFromZakToEnd.TotalMilliseconds);
                zakToEndTimer.Tick += new EventHandler(TimerEventProcessorEnd);
                zakToEndTimer.Start();
            }

        }


        private static void changeId(int id)
        {
            idForMethods = id;
        }
        private static void TimerEventProcessorZak(Object myObject,EventArgs myEventArgs)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getReservation = context.Rezerwacja_pokoi.Where(i => i.Rezerwacja_pokoju_id == idForMethods).FirstOrDefault();
            if (getReservation.Status_id == 2)
            {
                Powiadomienie p = new Powiadomienie(getReservation.Rezerwacja_pokoju_id, false);
                p.Show();
                rezToZakTimer.Stop();
            }

        }
        private static void TimerEventProcessorEnd(Object myObject, EventArgs myEventArgs)
        {
            Hotel_u_ZbysiaDBEntities context = new Hotel_u_ZbysiaDBEntities();
            var getReservation = context.Rezerwacja_pokoi.Where(i => i.Rezerwacja_pokoju_id == idForMethods).FirstOrDefault();
            if (getReservation.Status_id == 1)
            {
                Powiadomienie p = new Powiadomienie(getReservation.Rezerwacja_pokoju_id, true);
                p.Show();
                zakToEndTimer.Stop();
            }
        }
    }
}
