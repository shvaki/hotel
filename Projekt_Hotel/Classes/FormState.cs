﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt_Hotel.Forms.Hotel_Form;
using Projekt_Hotel.Forms.Login_Form;
using Projekt_Hotel.Forms.Massage_Form;
using Projekt_Hotel.Forms.Massage_Form.ListOfReservation;
using Projekt_Hotel.Forms.Start_Up_Form;

namespace Projekt_Hotel.Classes
{
    public static class FormState
    {
        public static Form PreviousPage;
        public static String NameOfPreviousButton;
        public static String NameOfLoggedPerson;

        //goto
        public static Form goToLoginForm = new LoginForm();
        public static Form goToHotelForm = new HotelForm();
        public static Form goToStartUpForm = new StartUpForm();
        public static Form goToMassageForm = new MassageForm();
        public static Form goToListOfReservationForm = new ListaRezerwacji();
    }
}
