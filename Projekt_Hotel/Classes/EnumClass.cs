﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Hotel.Classes
{
    public class EnumClass
    {
        public enum Status {
         	Zakwaterowano=1,
			Zarezerwowano=2,
			Zaplacono=3,
			Nie_zaplacono=4,
			Wolny=5,
			Zajety=6,
			Remont=7,
			Naprawa=8,
			Dziala=9,
			Anulowano=10
        }
    }
}
